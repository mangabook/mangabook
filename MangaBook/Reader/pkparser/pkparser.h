#ifndef _PKPARSER_H
#define _PKPARSER_H

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// struct taken from sweetscape's ZipTemplate for 010Editor
// 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "..\..\NekoDriverUnt.h"
#include <QtCore/QFile>

typedef unsigned short ushort;
typedef ushort DOSTIME;
typedef ushort DOSDATE;
typedef unsigned int uint;

//enum used for compression format
typedef enum { 
    COMP_STORED    = 0,
    COMP_SHRUNK    = 1,
    COMP_REDUCED1  = 2,
    COMP_REDUCED2  = 3,
    COMP_REDUCED3  = 4,
    COMP_REDUCED4  = 5,
    COMP_IMPLODED  = 6,
    COMP_TOKEN     = 7,
    COMP_DEFLATE   = 8,
    COMP_DEFLATE64 = 9    
} COMPTYPE;

// Defines a file record
typedef struct  tagZIPFILERECORD {
    // Header for the file
    char     frSignature[4];    //0x04034b50
    ushort   frVersion;
    ushort   frFlags;
    COMPTYPE frCompression;
    DOSTIME  frFileTime;
    DOSDATE  frFileDate;
    uint     frCRC;
    uint     frCompressedSize;
    uint     frUncompressedSize;
    ushort   frFileNameLength;
    ushort   frExtraFieldLength;

    // Hybrid
    char*   frFileName;
    uchar*  frExtraField;

    // Compressed data
    uchar*  frData;

    // Data descriptor
    uint     frCrc2;
    uint     frCompressedSize2;
    uint     frUncompressedSize2;

    int ParseStdBytes(QFile& file, bool touchonly = false);
    int ParseExtraBytes(QFile& file, bool touchonly = true);

    tagZIPFILERECORD();
    ~tagZIPFILERECORD();
/*
    if( frFileNameLength > 0 )
        char     frFileName[ frFileNameLength ];
    if( frExtraFieldLength > 0 )
        uchar    frExtraField[ frExtraFieldLength ];

    // Compressed data
    SetBackColor( cNone );
    if( frCompressedSize > 0 )
        uchar    frData[ frCompressedSize ];

    // Data descriptor
    if( frFlags & 0x04 )
    {
        SetBackColor( cLtAqua );
        uint     frCrc2;
        uint     frCompressedSize2;
        uint     frUncompressedSize2;
    }
*/
} ZIPFILERECORD;

// Defines an entry in the directory table
typedef struct tagZIPDIRENTRY {
    char     deSignature[4];     //0x02014b50
    ushort   deVersionMadeBy;
    ushort   deVersionToExtract;
    ushort   deFlags;
    COMPTYPE deCompression;
    DOSTIME  deFileTime;
    DOSDATE  deFileDate;
    uint     deCRC;
    uint     deCompressedSize;
    uint     deUncompressedSize;
    ushort   deFileNameLength;
    ushort   deExtraFieldLength;
    ushort   deFileCommentLength;
    ushort   deDiskNumberStart;
    ushort   deInternalAttributes;
    uint     deExternalAttributes;
    uint     deHeaderOffset;

    char*    deFileName;
    uchar*   deExtraField;
    uchar*   deFileComment;

    int ParseStdBytes(QFile& file, bool touchonly = false);
    int ParseExtraBytes(QFile& file, bool touchonly = true);

    tagZIPDIRENTRY();
    ~tagZIPDIRENTRY();

    /*if( deFileNameLength > 0 )
        char     deFileName[ deFileNameLength ];
    if( deExtraFieldLength > 0 )
        uchar    deExtraField[ deExtraFieldLength ];
    if( deFileCommentLength > 0 )
        uchar    deFileComment[ deFileCommentLength ];*/
} ZIPDIRENTRY;

// Defines the digital signature
typedef struct tagZIPDIGITALSIG {
    char     dsSignature[4];    //0x05054b50
    ushort   dsDataLength;

    uchar*   dsData;

    int ParseStdBytes(QFile& file, bool touchonly = false);
    int ParseExtraBytes(QFile& file, bool touchonly = true);

    tagZIPDIGITALSIG();
    ~tagZIPDIGITALSIG();

    /*if( dsDataLength > 0 )
        uchar    dsData[ dsDataLength ];*/
} ZIPDIGITALSIG;

// Defines the Data descriptor
typedef struct tagZIPDATADESCR {
    char ddSignature[4]; //0x08074b50
    uint ddCRC;
    uint ddCompressedSize;
    uint ddUncompressedSize;
} ZIPDATADESCR;

// Defines the end of central directory locater
typedef struct tagZIPENDLOCATOR {
    char     elSignature[4];    //0x06054b50
    ushort   elDiskNumber;
    ushort   elStartDiskNumber;
    ushort   elEntriesOnDisk;
    ushort   elEntriesInDirectory;
    uint     elDirectorySize;
    uint     elDirectoryOffset;
    ushort   elCommentLength;

    char*    elComment;

    int ParseStdBytes(QFile& file, bool touchonly = false);
    int ParseExtraBytes(QFile& file, bool touchonly = true);

    tagZIPENDLOCATOR();
    ~tagZIPENDLOCATOR();

    /*if( elCommentLength > 0 )
        char    elComment[ elCommentLength ]*/;
} ZIPENDLOCATOR;

bool BuildPKArchiveScrollList(QString pkfilename, PageBundleList& scrollfiles);

#endif