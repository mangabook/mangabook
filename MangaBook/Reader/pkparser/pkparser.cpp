#include "pkparser.h"
#include "..\..\AddonFuncUnt.h"
#include <algorithm>
#include "..\..\Common.h"

bool FilenameCompareAsc(const TPageBundleRec& rec1, const TPageBundleRec& rec2) {
    return rec1.Fullpath.compare(rec2.Fullpath) < 0;
}

bool BuildPKArchiveScrollList(QString pkfilename, PageBundleList& scrollfiles) {
    bool Result = false;
    QFile file(pkfilename);
    file.open(QIODevice::ReadOnly);

    if (file.isReadable() == false) {
        return Result;
    }

    uint tag;

    while (file.atEnd() == false)
    {
        tag = readuint32(file, false, true);
        if( tag == 0x04034b50 )
        {
            //SetBackColor( cLtGray );
            ZIPFILERECORD fileRecord; // We Store it
            fileRecord.ParseStdBytes(file, false);
            fileRecord.ParseExtraBytes(file, false);
            
            TPageBundleRec item;
            item.Fullpath = QString("%1|%2").arg(pkfilename).arg(QString::fromUtf16((ushort*)StringToWideString(fileRecord.frFileName).c_str())); // TODO: Guess CodePage
            item.Semipath = QString::fromUtf16((ushort*)StringToWideString(fileRecord.frFileName).c_str());
            //item.filesize = fileRecord.frUncompressedSize;
            //*LPDWORD(LPBYTE(&item.filesize) + 4)  = nFileSizeHigh;
            //item.filetimestamp = ftLastWriteTime;
            item.FromArchive = true;
            item.Folderpath = pkfilename;
            item.StreamOffset = file.pos() - fileRecord.frCompressedSize;
            item.StreamSize = fileRecord.frCompressedSize;
            item.SourceSize = fileRecord.frUncompressedSize;
            if (fileRecord.frCompression == COMP_STORED) {
                item.StreamCodec = scStored;
            } else if (fileRecord.frCompression == COMP_DEFLATE) {
                item.StreamCodec = scDeflate;
            } else if (fileRecord.frCompression == COMP_DEFLATE64) {
                item.StreamCodec = scDeflate64;
            } else {
                item.StreamCodec = scUnknown;
            }
            scrollfiles.push_back(item);
        }
        else if( tag == 0x02014b50 )
        {
            //SetBackColor( cLtPurple );
            ZIPDIRENTRY dirEntry; // we drop it
            dirEntry.ParseStdBytes(file, false);
            dirEntry.ParseExtraBytes(file, true);
        }
        else if( tag == 0x05054b50 )
        {
            //SetBackColor( cLtBlue );
            ZIPDIGITALSIG digitalSig; // we omit it
            digitalSig.ParseStdBytes(file, false);
            digitalSig.ParseExtraBytes(file, true);
        }
        else if( tag == 0x06054b50 )
        {
            //SetBackColor( cLtYellow );
            ZIPENDLOCATOR endLocator; // we wait for it
            endLocator.ParseStdBytes(file, false);
            endLocator.ParseExtraBytes(file, true);
        }
        else
        {
            //Warning( "Unknown ZIP tag encountered. Template stopped." );
            return Result;
        }
    }

    // TODO: Smart Sort
    qSort(scrollfiles.begin(), scrollfiles.end(), FilenameCompareAsc);

    return Result;
}

int ZIPFILERECORD::ParseExtraBytes( QFile& file, bool touchonly )
{
    int Result = 0;
    if (frFileNameLength > 0) {
        Result += frFileNameLength;
        if (touchonly) {
            //file.seek(file.pos() + frFileNameLength);
        } else {
            QByteArray filename = file.read(frFileNameLength);
            frFileName = new char[filename.count() + 1];
            strncpy_s(frFileName, filename.count() + 1, filename.data(), filename.count());
        }
    }
    if (frExtraFieldLength > 0) {
        Result += frExtraFieldLength;
    }
    if (frCompressedSize > 0) {
        Result += frCompressedSize;
    }
    if (frFlags & 0x04) {
        Result += sizeof(frCrc2) 
            + sizeof(frCompressedSize2)
            + sizeof(frUncompressedSize2);
    }
    if (touchonly) {
        file.seek(file.pos() + Result);
    } else {
        file.seek(file.pos() + Result - frFileNameLength);
    }
    return Result;
}

int tagZIPFILERECORD::ParseStdBytes( QFile& file, bool touchonly )
{
    int Result = 0;

    Result += sizeof(frSignature)
        + sizeof(frVersion)
        + sizeof(frFlags)
        + sizeof(frCompression)
        + sizeof(frFileTime)
        + sizeof(frFileDate)
        + sizeof(frCRC)
        + sizeof(frCompressedSize)
        + sizeof(frUncompressedSize)
        + sizeof(frFileNameLength)
        + sizeof(frExtraFieldLength);

    if (touchonly) {
        file.seek(file.pos() + Result);
        return Result;
    }

    *LPDWORD(&frSignature[0]) = readuint32(file);
    frVersion   = readuint16(file);
    frFlags     = readuint16(file);
    frCompression = COMPTYPE(readuint16(file)); // TODO: ushort it
    frFileTime  = readuint16(file);
    frFileDate  = readuint16(file);
    frCRC       = readuint32(file);
    frCompressedSize = readuint32(file);
    frUncompressedSize = readuint32(file);
    frFileNameLength = readuint16(file);
    frExtraFieldLength = readuint16(file);

    return Result;
}

tagZIPFILERECORD::tagZIPFILERECORD()
{
    memset(this, 0, sizeof(tagZIPFILERECORD));
}

tagZIPFILERECORD::~tagZIPFILERECORD()
{
    if (frFileName) {
        delete frFileName;
    }
    if (frExtraField) {
        delete frExtraField; // never
    }
    if (frData) {
        delete frData; // never
    }
}

int tagZIPDIRENTRY::ParseStdBytes( QFile& file, bool touchonly /*= false*/ )
{
    int Result = 0;

    Result += sizeof(deSignature)
        + sizeof(deVersionMadeBy)
        + sizeof(deVersionToExtract)
        + sizeof(deFlags)
        + sizeof(deCompression)
        + sizeof(deFileTime)
        + sizeof(deFileDate)
        + sizeof(deCRC)
        + sizeof(deCompressedSize)
        + sizeof(deUncompressedSize)
        + sizeof(deFileNameLength)
        + sizeof(deExtraFieldLength)
        + sizeof(deFileCommentLength)
        + sizeof(deDiskNumberStart)
        + sizeof(deInternalAttributes)
        + sizeof(deExternalAttributes)
        + sizeof(deHeaderOffset);

    if (touchonly) {
        file.seek(file.pos() + Result);
        return Result;
    }

    *LPDWORD(&deSignature[0]) = readuint32(file);
    deVersionMadeBy = readuint16(file);
    deVersionToExtract = readuint16(file);
    deFlags = readuint16(file);
    deCompression = COMPTYPE(readuint16(file));
    deFileTime = readuint16(file);
    deFileDate = readuint16(file);
    deCRC = readuint32(file);
    deCompressedSize = readuint32(file);
    deUncompressedSize = readuint32(file);
    deFileNameLength = readuint16(file);
    deExtraFieldLength = readuint16(file);
    deFileCommentLength = readuint16(file);
    deDiskNumberStart = readuint16(file);
    deInternalAttributes = readuint16(file);
    deExternalAttributes = readuint32(file);
    deHeaderOffset = readuint32(file);

    return Result;
}

int tagZIPDIRENTRY::ParseExtraBytes( QFile& file, bool touchonly /*= true*/ )
{
    int Result = 0;
    if (deFileNameLength > 0) {
        Result += deFileNameLength;
        if (touchonly) {
            //file.seek(file.pos() + deFileNameLength);
        } else {
            QByteArray filename = file.read(deFileNameLength);
            deFileName = new char[filename.count() + 1];
            strncpy_s(deFileName, filename.count() + 1, filename.data(), filename.count());
        }
    }
    if (deExtraFieldLength > 0) {
        Result += deExtraFieldLength;
    }
    if (deFileCommentLength > 0) {
        Result += deFileCommentLength;
    }
    if (touchonly) {
        file.seek(file.pos() + Result);
    } else {
        file.seek(file.pos() + Result - deFileNameLength);
    }
    return Result;
}

tagZIPDIRENTRY::tagZIPDIRENTRY()
{
    memset(this, 0, sizeof(tagZIPDIRENTRY));
}

tagZIPDIRENTRY::~tagZIPDIRENTRY()
{
    if (deFileName) {
        delete deFileName;
    }
    if (deExtraField) {
        delete deExtraField; // never
    }
    if (deFileComment) {
        delete deFileComment; // never
    }

}

int tagZIPDIGITALSIG::ParseStdBytes( QFile& file, bool touchonly /*= false*/ )
{
    int Result = 0;

    Result += sizeof(dsSignature)
        + sizeof(dsDataLength);

    if (touchonly) {
        file.seek(file.pos() + Result);
        return Result;
    }

    *LPDWORD(&dsSignature[0]) = readuint32(file);
    dsDataLength = readuint32(file);

    return Result;
}

int tagZIPDIGITALSIG::ParseExtraBytes( QFile& file, bool touchonly /*= true*/ )
{
    int Result = 0;
    if (dsDataLength > 0) {
        Result += dsDataLength;
        file.seek(file.pos() + dsDataLength);
    }
    return Result;
}

tagZIPDIGITALSIG::tagZIPDIGITALSIG()
{
    memset(this, 0, sizeof(tagZIPDIGITALSIG));
}

tagZIPDIGITALSIG::~tagZIPDIGITALSIG()
{
    if (dsData) {
        delete dsData; // never
    }
}

int tagZIPENDLOCATOR::ParseStdBytes( QFile& file, bool touchonly /*= false*/ )
{
    int Result = 0;

    Result += sizeof(elSignature)
        + sizeof(elDiskNumber)
        + sizeof(elStartDiskNumber)
        + sizeof(elEntriesOnDisk)
        + sizeof(elEntriesInDirectory)
        + sizeof(elDirectorySize)
        + sizeof(elDirectoryOffset)
        + sizeof(elCommentLength);

    if (touchonly) {
        file.seek(file.pos() + Result);
        return Result;
    }

    *LPDWORD(&elSignature[0]) = readuint32(file);
    elDiskNumber = readuint16(file);
    elStartDiskNumber = readuint16(file);
    elEntriesOnDisk = readuint16(file);
    elEntriesInDirectory = readuint16(file);
    elDirectorySize = readuint32(file);
    elDirectoryOffset = readuint32(file);
    elCommentLength = readuint16(file);

    return Result;

}

int tagZIPENDLOCATOR::ParseExtraBytes( QFile& file, bool touchonly /*= true*/ )
{
    int Result = 0;
    if (elCommentLength > 0) {
        Result += elCommentLength;
        file.seek(file.pos() + elCommentLength);
    }
    return Result;
}

tagZIPENDLOCATOR::tagZIPENDLOCATOR()
{
    memset(this, 0, sizeof(tagZIPENDLOCATOR));
}

tagZIPENDLOCATOR::~tagZIPENDLOCATOR()
{
    if (elComment) {
        delete elComment; // never
    }
}
