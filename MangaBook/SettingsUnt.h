#ifndef SETTINGSUNT_H
#define SETTINGSUNT_H

#include <QDialog>
#include "DbCentre.h"

namespace Ui {
    class TSettingsFrm;
}

class TSettingsFrm : public QDialog
{
    Q_OBJECT

public:
    explicit TSettingsFrm(QWidget *parent = 0);
    ~TSettingsFrm();

private:
    Ui::TSettingsFrm *ui;
    TGlobalRecItem fGlobalSettings;
private slots:
    void onPageSplitValueChanged(int newindex);
public:
    void StoreGlobalInfo(const TGlobalRecItem& info);
    TGlobalRecItem GetGlobalInfo();
};

#endif // SETTINGSUNT_H
