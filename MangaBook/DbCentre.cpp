#include <QtGui/QApplication>
#include <QtCore/QSettings>
#include <QtCore/QFile>
#include <QtSql/QtSQL>
#include "DbCentre.h"

TPathRecItem PathSetting; // imp
TStateRecItem StateSetting; // imp
TGlobalRecItem GlobalSetting;

void LoadAppSettings( void )
{
    QSettings settings(QApplication::applicationDirPath() + "/ConfHonya.ini",
        QSettings::IniFormat);
    PathSetting.LastSourceFolder = settings.value("Path/LastSourceFolder", "").toString();
    PathSetting.LastStockBookFolder = settings.value("Path/LastStockBookFolder", "").toString();
    PathSetting.LastCustomBookFolder = settings.value("Path/LastCustomBookFolder", "").toString();
    PathSetting.LastExportFolder = settings.value("Path/LastExportFolder", "").toString();
    PathSetting.LastImportFolder = settings.value("Path/LastImportFolder", "").toString();
    PathSetting.LastProjectFolder = settings.value("Path/LastProjectFolder", "").toString();
    PathSetting.LastSelectedItemIndex = settings.value("Path/LastSelectedItemIndex", 0).toInt();

    StateSetting.WindowMaxium = settings.value("State/WindowMaxium", true).toBool();
    StateSetting.MainFrmState = settings.value("State/MainFrmState", QByteArray()).toByteArray();
    StateSetting.ProjectLayoutState = settings.value("State/ProjectLayoutState", QByteArray()).toByteArray();
    StateSetting.MessageLayoutState = settings.value("State/MessageLayoutState", QByteArray()).toByteArray();

    StateSetting.RegEditorMaxium = settings.value("State/RegEditorMaxium", true).toBool();
    StateSetting.RegFrmState = settings.value("State/RegEditorMaxium", QByteArray()).toByteArray();
    StateSetting.KeyLayoutState = settings.value("State/KeyLayoutState", QByteArray()).toByteArray();

    GlobalSetting.AutoCrop = settings.value("Global/AutoCrop", true).toBool();
    GlobalSetting.AutoExposure = settings.value("Global/AutoCrop", true).toBool();
    GlobalSetting.SplitPage = MangaDoublePageMode(settings.value("Global/SplitPage", mdpmAutoNippon).toInt());
    GlobalSetting.PageSorting = PageSortingMethod(settings.value("Global/PageSorting", psmAutomatic).toInt());
    GlobalSetting.EnhanceLevel = FilterMode(settings.value("Global/EnhanceLevel", fmHigh).toInt());
    GlobalSetting.OutputFormat = CodecMode(settings.value("Global/OutputFormat", cmJPEGVeryHigh).toInt());
    GlobalSetting.OutputResolution = ResizeMode(settings.value("Global/OutputResolution", rmFullScreen).toInt());
    GlobalSetting.UseSurfaceBlur = settings.value("Global/UseSurfaceBlur", false).toBool();
    GlobalSetting.RemoveJPEGArtifacts = settings.value("Global/RemoveJPEGArtifacts", false).toBool();
}

void SaveAppSettings( void )
{
    QSettings settings(QApplication::applicationDirPath() + "/ConfHonya.ini",
        QSettings::IniFormat);
    settings.beginGroup("Path");
    settings.setValue("LastSourceFolder", PathSetting.LastSourceFolder);
    settings.setValue("LastStockBookFolder", PathSetting.LastStockBookFolder);
    settings.setValue("LastCustomBookFolder", PathSetting.LastCustomBookFolder);
    settings.setValue("LastExportFolder", PathSetting.LastExportFolder);
    settings.setValue("LastImportFolder", PathSetting.LastImportFolder);
    settings.setValue("LastProjectFolder", PathSetting.LastProjectFolder);
    settings.setValue("LastSelectedItemIndex", PathSetting.LastSelectedItemIndex);
    settings.endGroup();

    settings.beginGroup("State");
    settings.setValue("WindowMaxium", StateSetting.WindowMaxium);
    settings.setValue("MainFrmState", StateSetting.MainFrmState);
    settings.setValue("ProjectLayoutState", StateSetting.ProjectLayoutState);
    settings.setValue("MessageLayoutState", StateSetting.MessageLayoutState);

    settings.setValue("RegEditorMaxium", StateSetting.RegEditorMaxium);
    settings.setValue("RegFrmState", StateSetting.RegFrmState);
    settings.setValue("KeyLayoutState", StateSetting.KeyLayoutState);
    settings.endGroup();

    settings.beginGroup("Global");
    settings.setValue("AutoCrop", GlobalSetting.AutoCrop);
    settings.setValue("AutoExposure", GlobalSetting.AutoExposure);
    settings.setValue("SplitPage", GlobalSetting.SplitPage);
    settings.setValue("PageSorting", GlobalSetting.PageSorting);
    settings.setValue("EnhanceLevel", GlobalSetting.EnhanceLevel);
    settings.setValue("OutputFormat", GlobalSetting.OutputFormat);
    settings.setValue("OutputResolution", GlobalSetting.OutputResolution);
    settings.setValue("UseSurfaceBlur", GlobalSetting.UseSurfaceBlur);
    settings.setValue("RemoveJPEGArtifacts", GlobalSetting.RemoveJPEGArtifacts);
    settings.endGroup();
}

bool BuildWorkspaceConnection( QString filename, QSqlDatabase& db )
{
    if (db.isValid() == false) {
        db = QSqlDatabase::addDatabase("QSQLITE", "connSQLite");
    }
    db.setDatabaseName(filename);
    return db.open();
}

void BreakWorkspaceConnection(QSqlDatabase& db)
{
    db.close();
    db.setDatabaseName("");
    db.removeDatabase("connSQLite");
    QSqlDatabase::removeDatabase("connSQLite");
}

bool AutoAppendSeparator( QString& source, QString separator )
{
    if (source.isEmpty() == false) {
        source += separator;
        return true;
    }
    return false;
}

QDataStream & operator<<( QDataStream &out, const TBookBundleRec &book )
{
    out << book.BookTitle << book.Author << book.Generator << book.Brief;
    bool havecover = (book.Cover != NULL);
    out << havecover;
    if (havecover) {
        out << *book.Cover;
    }
    return out;
}

QDataStream & operator<<( QDataStream &out, const TChapterBundleRec &chapter )
{
    out << chapter.Chaptername << chapter.Pages << chapter.NewChapterIndex << chapter.DoublePageMode;
    return out;
}

QDataStream & operator<<( QDataStream &out, const TPageBundleRec &page )
{
    out << page.Fullpath; // C:\d.jpg, a.zip|a/b.jpg
    out << page.Semipath; // d, b
    out << page.Folderpath; // C:\, a.zip
    out << page.FromArchive << page.FromGrayscale <<  page.FromAnimate << page.FitRotated1 << page.FitRotated2 << page.DoublePage << page.Sharpened << page.Corrupted << page.Busy;
    out << page.StreamOffset << page.StreamSize << page.SourceSize;
    out << page.StreamCodec;
    // FitImage1, FitFile1, FitJPEG1 is only used in building process
    // ExtraInfo is write only
    out << page.DoublePageMode;

    return out;
}

QDataStream & operator>>( QDataStream &in, TBookBundleRec &book )
{
    in >> book.BookTitle >> book.Author >> book.Generator >> book.Brief;
    bool havecover;
    in >> havecover;
    if (havecover) {
        if (book.Cover == NULL) {
            book.Cover = new QImage(); // only alloc
        }
        in >> *book.Cover;
    }
    return in;
}

QDataStream & operator>>( QDataStream &in, TChapterBundleRec &chapter )
{
    in >> chapter.Chaptername >> chapter.Pages >> chapter.NewChapterIndex >> chapter.DoublePageMode;
    return in;
}

QDataStream & operator>>( QDataStream &in, TPageBundleRec &page )
{
    in >> page.Fullpath; // C:\d.jpg, a.zip|a/b.jpg
    in >> page.Semipath; // d, b.jpg
    in >> page.Folderpath; // C:\, a.zip
    in >> page.FromArchive >> page.FromGrayscale >>  page.FromAnimate >> page.FitRotated1 >> page.FitRotated2 >> page.DoublePage >> page.Sharpened >> page.Corrupted >> page.Busy;
    in >> page.StreamOffset >> page.StreamSize >> page.SourceSize;
    in >> page.StreamCodec;
    // FitImage1, FitFile1, FitJPEG1 is only used in building process
    // ExtraInfo is write only
    in >> page.DoublePageMode;

    return in;
}

QDataStream & operator<<( QDataStream &out, const MangaDoublePageMode &mode )
{
    out << quint32(mode);
    return out;
}

QDataStream & operator>>( QDataStream &in, MangaDoublePageMode &mode )
{
    quint32 dummy;
    in >> dummy;
    mode = MangaDoublePageMode(dummy);
    return in;
}

QDataStream & operator<<( QDataStream &out, const ScaleFilter &filter )
{
    out << quint32(filter);
    return out;
}

QDataStream & operator>>( QDataStream &in, ScaleFilter &filter )
{
    quint32 dummy;
    in >> dummy;
    filter = ScaleFilter(dummy);
    return in;
}

QDataStream & operator<<( QDataStream &out, const TStreamCodec &codec )
{
    out << quint32(codec);
    return out;
}

QDataStream & operator>>( QDataStream &in, TStreamCodec &codec )
{
    quint32 dummy;
    in >> dummy;
    codec = TStreamCodec(dummy);
    return in;
}