#include "NekoDriverUnt.h"
#include "AddonFuncUnt.h"
#include "Filter/QBaseFilter.h"
#include "Filter/QResample.h"
#include "Filter/waveletsharpen/sharpen.h"
#include <QtCore/QThread>
#include "DbCentre.h"


void TNekoDriver::BuildSinglePrefetch( TPageBundleRec& page, int destwidth, int destheight, ScaleFilter filter, MangaDoublePageMode doublepagemode, bool crop, bool exposure, bool surface, bool artifacts, bool gamma, bool sharpen )
{
    if (page.FromArchive) {
        if (QFileInfo(page.Folderpath).exists() == false) {
            return;
        }
    } else {
        if (QFileInfo(page.Fullpath).exists() == false) {
            return;
        }
    }

    TImageType mimetype;
    if (page.FromArchive) {
        mimetype = itPKArchive;
    } else {
        mimetype = GuessMIMEType(page.Fullpath);
    }

    //VERBOSEMSG(true, (L"Locked fBuildingcs in BuildSinglePrefetch (0x%x).\n", GetCurrentThreadId()));
    QImageReader* reader;
    if (page.FromArchive) {
        QFile pkfile(page.Folderpath);
        pkfile.open(QIODevice::ReadOnly);
        pkfile.seek(page.StreamOffset);
        quint32 sourcesize = page.SourceSize;
        QByteArray packedstream = pkfile.read(page.StreamSize);
        QBuffer *uncompressbuf = new QBuffer(); // prevent refcount
        pkfile.close();
        if (page.StreamCodec == scStored) {
            uncompressbuf->setData(packedstream);
        } else if (page.StreamCodec == scDeflate) {
            uncompressbuf->setData(qUncompressEx((uchar*)packedstream.constData(), packedstream.size(), sourcesize));
        }
        mimetype = GuessMIMEType(uncompressbuf);
        uncompressbuf->open(QBuffer::ReadOnly);

        reader = new QImageReader(uncompressbuf);
    } else {
        reader = new QImageReader(page.Fullpath, MIMETypeToExt(mimetype).toAscii());
    }

    // pushdown
    int loadtime = -1, scaletime = -1;
    int orgwidth, orgheight;
    QImage qimage, qimage2;

    QSize orgsize = reader->size();
    orgwidth = orgsize.width();
    orgheight = orgsize.height();
    if (mimetype == itJPEG) {
        // TODO: thumbnail
    }

    if (mimetype == itPNG || mimetype == itJPEG) {
        if (NicetoScaleOnFly(orgsize, mimetype == itPNG, mimetype == itJPEG)) {
            reader->setScaledSize(orgsize);
            reader->setQuality(49);
        }
    }
    int starttime = GetTickCount();
    int lastmem = GetFreePhysMemory();
    if (reader->read(&qimage) == false) {
        qimage = QImage();
    }
    loadtime = GetTickCount() - starttime;
    if (reader->supportsAnimation() && reader->canRead()) {
        // TODO: size detection
        page.FromAnimate = true;
        //page.AnimateStore = reader;
    } else {
        //delete reader;
    }
    if (page.FromArchive && reader->device()) {
        delete (QBuffer*)reader->device();
        reader->setDevice(NULL);
    }
    delete reader;

    // if continues. need Leave Building cs
    int width, height;
    if (qimage.isNull() == false) {
        width = qimage.width();
        height = qimage.height();
        // qimage may have wild size in insertion mode
        if (doublepagemode != mdpmSingle) {
            // 600x800=1.5, 103x182=1.13
            // TODO: size detect
            page.DoublePage = ((float(width) / height > 1.18) && (float(width) / height < 2.5));
            if ((width < destwidth && height < destheight) || (width < destheight && height < destwidth)) {
                page.DoublePage = false;
            }
            //qDebug() << "DoublePage:" << page.DoublePage << width << "x" << height;
        } else {
            page.DoublePage = false;
        }
        if (page.FitImage1 == NULL) {
            // reset
            page.FitRotated1 = false;
        }
        if (page.FitImage2 == NULL) {
            // reset
            page.FitRotated2 = false;
        }
        // TODO: add argument
        if (width > height) {
            if (page.DoublePage) {
                // 721 * 961
                if (width > destheight * 2 || height > destwidth) {
                    // we wanna an 720 * 960 90 rotated picture without any border optimize
                    double overcopy = 0.5;
                    if (doublepagemode == mdpmSmartNippon || doublepagemode == mdpmSmartNative) {
                        // TODO: detect real guide position
                        overcopy = 0.515;
                    }
                    if (doublepagemode == mdpmAutoNippon || doublepagemode == mdpmSmartNippon) {
                        // right to left
                        qimage2 = qimage.copy(0, 0, qimage.width() * overcopy, qimage.height());
                        qimage = qimage.copy(qimage.width() - qimage.width() * overcopy, 0, qimage.width() * overcopy, qimage.height());
                    }
                    if (doublepagemode == mdpmAutoNative || doublepagemode == mdpmSmartNative) {
                        qimage2 = qimage.copy(qimage.width() - qimage.width() * overcopy, 0, qimage.width() * overcopy, qimage.height());
                        qimage = qimage.copy(0, 0, qimage.width() * overcopy, qimage.height());
                    }
                    if (crop) {
                        AutocropImage(qimage, true, doublepagemode == mdpmAutoNative || doublepagemode == mdpmSmartNative);
                        AutocropImage(qimage2, true, doublepagemode == mdpmAutoNippon || doublepagemode == mdpmSmartNippon);
                    }
                    if (exposure) {
                        AutoExposureImage(qimage, true);
                        AutoExposureImage(qimage2, true);
                    }
                    page.FitRotated1 = NiceToRotate(qimage.width(), qimage.height(), destwidth, destheight);
                    PrepareScaledQPicture(qimage, destwidth, destheight, filter, scaletime); // auto rotate
                    page.FitRotated2 = NiceToRotate(qimage2.width(), qimage2.height(), destwidth, destheight);
                    PrepareScaledQPicture(qimage2, destwidth, destheight, filter, scaletime); // auto rotate
                }
                // else only keep and wait for black border.
            } else if (height - destheight > 80 || width - destwidth > 120) {
                // Crop Scale 800x600 -> 720x480 may not rotated
                if (crop) {
                    AutocropImage(qimage, false, false);
                }
                if (exposure) {
                    AutoExposureImage(qimage, true);
                }
                page.FitRotated1 = NiceToRotate(width, height, destwidth, destheight);
                PrepareScaledQPicture(qimage, destwidth, destheight, filter, scaletime); // auto rotate
            }
        } else {
            // width <= height, source is portrait
            // if we get qimage from prefetch and less than 720*960, do nothing
            if (page.DoublePage) {
                // never split portrait page?
                if (width <= destwidth && height <= destheight * 2) {
                    // less than 720 * 960, ok
                } else {
                    page.FitRotated1 = NiceToRotate(qimage.width(), qimage.height(), destwidth, destheight * 2);
                    PrepareScaledQPicture(qimage, destwidth, destheight * 2, filter, scaletime); // auto rotate
                }
            } else if ((destwidth > destheight && (width - destheight > 20 || height - destwidth > 30)) || (destheight > destwidth && (width - destwidth > 20 || height - destheight > 30))) {
                // Crop Scale 600x800 -> 720x480, sometimes rotated
                if (crop) {
                    AutocropImage(qimage, false, false);
                }
                if (exposure) {
                    AutoExposureImage(qimage, true);
                }
                page.FitRotated1 = NiceToRotate(width, height, destwidth, destheight);
                PrepareScaledQPicture(qimage, destwidth, destheight, filter, scaletime); // auto rotate
            } else {
                // Safe for manual Rotate?
                // Cute image , 200x400 -> 480x720
                // TODO: Design by M8 default rotation
                page.FitRotated1 = false;
            }
        }

        // PostFilter, Apply on splitted n scaled pages
        bool expired = false;
        if (gamma) {
            //changeContrast(qimage, 108);
            //changeBrightness(qimage, -2);
            changeGamma(qimage, 120);
            if (page.DoublePage) {
                changeGamma(qimage2, 120);
            }
        }
        if (sharpen && page.Sharpened == false) {
            // TODO: AutoLevel, SurfaceBlur, AWarpSharpen
            ::sharpen(qimage, 0.4, 0.2, true);
            if (page.DoublePage) {
                ::sharpen(qimage2, 0.4, 0.2, true);
            }
            expired = true;
        }
        // Store fitimage for next. we only store uncrop unchecker qimage
        if (page.FitImage1 == NULL || (doublepagemode != mdpmSingle && page.FitImage2 == NULL) || expired) {
            // TODO: QAtomicPointer
            QImage *smart = qimage.isNull()?NULL:new QImage(qimage); // Duplicate
            (void) InterlockedExchange((long*)&page.FitImage1, (long)smart);
            if (page.DoublePage) {
                smart = qimage2.isNull()?NULL:new QImage(qimage2);
                (void) InterlockedExchange((long*)&page.FitImage2, (long)smart);
            }
            page.DoublePageMode = doublepagemode;
            page.Sharpened = sharpen;
            page.ExtraInfo.originalwidth = orgwidth;
            page.ExtraInfo.originalheight = orgheight;
            page.ExtraInfo.loadtime = loadtime;
            page.ExtraInfo.scaletime = scaletime;
        }
    }
}

bool TNekoDriver::PrepareScaledQPicture( QImage& qimage, int destwidth, int destheight, ScaleFilter filter, int& scaletime )
{
    int width = qimage.width();
    int height = qimage.height();

    // Better Quality than in viaimage 
    if (qimage.depth() == 8) {
        Normalize8bitQImage(qimage);
    }

    bool rotate = NiceToRotate(width, height, destwidth, destheight);
    if (rotate) {
        qSwap(destwidth, destheight);
    }
    int starttime = GetTickCount();
    if (filter == sfLanczos3 || filter == sfQuadRatic) {
        // suite for manga
        QSize targetsize(qimage.size());
        targetsize.scale(destwidth, destheight, Qt::KeepAspectRatio);
        qimage = CreateResampledBitmap(qimage, targetsize.width(), targetsize.height(), filter == sfLanczos3?STOCK_FILTER_LANCZOS3:STOCK_FILTER_QUADRATIC);

    } else {
        // Qt == Auto
        Qt::TransformationMode scaleQ = double(width * height) / (destwidth * destheight) > 4? Qt::FastTransformation:Qt::SmoothTransformation;
        if (scaleQ == Qt::FastTransformation && double(width) / destheight > 2 && double(height) / destwidth > 2) {
            // never here
            qimage = qimage.scaled(width / 2, height / 2, Qt::KeepAspectRatio, scaleQ).scaled(destwidth, destheight, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        } else {
            qimage = qimage.scaled( destwidth, destheight,Qt::KeepAspectRatio, scaleQ);
        }
    }
    if (scaletime = -1) {
        scaletime = 0;
    }
    scaletime += GetTickCount() - starttime;

    if (rotate) {
        QMatrix matrix;
        matrix.rotate(-90); 
        qimage = qimage.transformed(matrix, Qt::FastTransformation);
    }
    return true;
}

bool TNekoDriver::NicetoScaleOnFly( QSize &orgsize, bool ispng, bool isjpeg  )
{
    bool cheated = false;

    return cheated;

    int orgwidth = orgsize.width();
    int orgheight = orgsize.height();
    if ((orgwidth > orgheight) && (orgwidth > 1400) && (orgheight >= 16)) {
        // 6000 * 2400?
        if (isjpeg) {
            // libjpeg8 + qjpegmod
            // 7/8 6/8 5/8 4/8 3/8 2/8 1/8
            for (int i = 7; i >= 1; i--) {
                if (greedydiv(orgwidth * i, 8) <= 1400) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth * i, 8));
                    orgsize.setHeight(greedydiv(orgheight * i, 8));
                    break;
                }
            }
        }
        if (ispng) {
            // please apply pngrez patch
            // 1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10
            for (int i = 2; i <= 16; i++) {
                if (greedydiv(orgwidth, i) <= 1400) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth, i));
                    orgsize.setHeight(greedydiv(orgheight, i));
                    break;
                }
            }
        }
    }
    if ((orgwidth < orgheight) && (orgwidth >=16) && (orgheight >= 1400)) {
        //  2400 * 6000?
        if (isjpeg) {
            // libjpeg7only
            // 7/8 6/8 5/8 4/8 3/8 2/8 1/8
            for (int i = 7; i >= 1; i--) {
                if (greedydiv(orgheight * i, 8) <= 1400) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth * i, 8));
                    orgsize.setHeight(greedydiv(orgheight * i, 8));
                    break;
                }
            }
        }
        if (ispng) {
            // please apply pngrez patch
            // 1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10
            for (int i = 2; i <= 16; i++) {
                if (greedydiv(orgheight, i) <= 1400) {
                    cheated = true;
                    orgsize.setWidth(greedydiv(orgwidth, i));
                    orgsize.setHeight(greedydiv(orgheight, i));
                    break;
                }
            }
        }
    }

    return cheated;
}

bool TNekoDriver::NiceToRotate( int width, int height, int destwidth, int destheight )
{
    //return fabs(double(width) / height - double(destheight) / destwidth) < 0.25 || double(height) / width - double(destwidth) / height > 0.5;
    return fabs(double(width) / height - double(destheight) / destwidth) < 0.25;
}

//void TNekoDriver::OptimizeMangaImage( QImage& qimage )
//{
//    changeContrast(qimage, 108);
//    changeBrightness(qimage, -2);
//    changeGamma(qimage, 120);
//    //QGaussFilter sharper;
//    //qimage = sharper.GetUnsharpMask(qimage, 0.5, 0.3);
//    sharpen(qimage, 0.4, 0.2, true);
//}

void ScalerThread::run()
{
    int chapterindex = 0;
    for(ChapterBundleList::iterator chapter = fChapters.begin(); chapter != fChapters.end(); chapter++) {
        int paegindex = 0;
        for(PageBundleList::iterator page = chapter->Pages.begin(); page != chapter->Pages.end(); page++) {
            if (page->FitImage1 == NULL && page->Corrupted == false && page->Busy == false) {
                // cost most time
                fChapterMutex.lock();
                if (page->Busy) {
                    fChapterMutex.unlock();
                    continue;
                }
                page->Busy = true;
                fChapterMutex.unlock();
                emit startScale(chapterindex, paegindex);
                //qDebug("Scaler 0x%x Get [%d][%d] to scale.\n", this->currentThreadId(), chapterindex, paegindex);
                TNekoDriver::BuildSinglePrefetch(*page, fStageWidth, fStageHeight, fScaleFilter, chapter->DoublePageMode, fAutoCrop, fAutoExposure, fSurfaceBlur, fRemoveArtifacts, fGammaCorrect, fSharpen);

                if (page->FitImage1 && page->Corrupted == false) {
#ifndef _DEBUG
                    UncropImage(*page->FitImage1, fStageWidth, fStageHeight);
#endif
                    QuantizeToGrayscaleQImage(*page->FitImage1, fDitherMode);
                }
                if (page->FitImage2 && page->Corrupted == false) {
#ifndef _DEBUG
                    UncropImage(*page->FitImage2, fStageWidth, fStageHeight);
#endif
                    QuantizeToGrayscaleQImage(*page->FitImage2, fDitherMode);
                }

                emit finishScale(chapterindex, paegindex);
            }
            paegindex++;
        }
        chapterindex++;
    }
    //qDebug("Scaler 0x%x outof work.\n", this->currentThreadId());
    this->deleteLater();
}

ScalerThread::ScalerThread( ChapterBundleList& chapters, QMutex& mutex, ScaleFilter filter, bool gamma, bool sharpen, bool dither, bool crop, bool exposure, bool surface, bool artifacts, int width, int height )
    : fChapters(chapters)
    , fChapterMutex(mutex)
    , fScaleFilter(filter)
    , fGammaCorrect(gamma)
    , fSharpen(sharpen)
    , fDitherMode(dither)
    , fAutoCrop(crop)
    , fAutoExposure(exposure)
    , fSurfaceBlur(surface)
    , fRemoveArtifacts(artifacts)
    , fStageWidth(width)
    , fStageHeight(height)
{

}

bool TNekoDriver::PreparePageImagesBuffer()
{
    bool result = true;
    int allpagecount = 0, readypagecount = 0;
    for(ChapterBundleList::iterator chapter = fChapters.begin(); chapter != fChapters.end(); chapter++) {
        int checkedpage = 0, landscapepage = 0;
        for(PageBundleList::iterator page = chapter->Pages.begin(); page != chapter->Pages.end(); page++) {
            if (page->FitImage1 == NULL && page->Corrupted == false) {
                page->Busy = false;
                allpagecount++;
                if (checkedpage < 10 && fDoublePageMode != mdpmSingle) {
                    if ((page->FromArchive  && QFileInfo(page->Folderpath).exists()) || (page->FromArchive == false && QFileInfo(page->Fullpath).exists())) {
                        TImageType mimetype;
                        if (page->FromArchive) {
                            mimetype = itPKArchive;
                        } else {
                            mimetype = GuessMIMEType(page->Fullpath);
                        }

                        double ratio = 0;
                        if (page->FromArchive) {
                            QFile pkfile(page->Folderpath);
                            pkfile.open(QIODevice::ReadOnly);
                            pkfile.seek(page->StreamOffset);
                            quint32 sourcesize = page->SourceSize;
                            QByteArray packedstream = pkfile.read(page->StreamSize);
                            QBuffer uncompressbuf;
                            pkfile.close();
                            if (page->StreamCodec == scStored) {
                                uncompressbuf.setData(packedstream);
                            } else if (page->StreamCodec == scDeflate) {
                                uncompressbuf.setData(qUncompressEx((uchar*)packedstream.constData(), packedstream.size(), sourcesize));
                            }
                            mimetype = GuessMIMEType(&uncompressbuf);
                            uncompressbuf.open(QBuffer::ReadOnly);

                            QImageReader *reader = new QImageReader(&uncompressbuf);
                            if (reader) {
                                ratio = double(reader->size().width()) / reader->size().height();
                                delete reader;
                            }
                        } else {
                            QImageReader *reader = new QImageReader(page->Fullpath, MIMETypeToExt(mimetype).toAscii());
                            if (reader) {
                                ratio = double(reader->size().width()) / reader->size().height();
                                delete reader;
                            }
                        }

                        // by pass narrow stripe
                        if (ratio > 0.5 && ratio < 2.5) {
                            // 0.9?
                            if (ratio > 1) {
                                landscapepage++;
                            }
                            checkedpage++;
                        }
                    } 

                }
            }
            if (page->FitImage1 && page->Corrupted == false) {
                readypagecount++;
            }
        }
        if (fDoublePageMode != mdpmSingle) {
            if (checkedpage == 0) {
                chapter->DoublePageMode = fDoublePageMode; // Auto or smart
            } else if (double(landscapepage) / checkedpage > 0.6) {
                chapter->DoublePageMode = fDoublePageMode; // Auto or smart
            } else {
                chapter->DoublePageMode = mdpmSingle;
            }
        } else {
        }
    }
    // TODO: Smart Double Page
    fBookDepository.CleanupAllBuffers(); // Cover Nuked
    fBookDepository.SetBookInfo(fBookRec.BookTitle, fBookRec.Author, fBookRec.Brief);
    if (fBookRec.Cover) {
        fBookDepository.SetCover(fBookRec.Cover);
    }

    fBusyPageCount = allpagecount;
    fFinishPageCount = 0;

    if (allpagecount == 0) {
        int processedpage = 0;
        for(ChapterBundleList::iterator chapter = fChapters.begin(); chapter != fChapters.end(); chapter++) {
            emit prepareBufferStageChanged(chapter->Chaptername);
            chapter->NewChapterIndex = fBookDepository.AddChapter(chapter->Chaptername);
            for(PageBundleList::iterator page = chapter->Pages.begin(); page != chapter->Pages.end(); page++) {
                if (page->FitImage1 && page->Corrupted == false) {
                    SaveBundleRec(*page);
                    fBookDepository.AddPageIntoChapter(chapter->NewChapterIndex, page->FitFile1, page->FitJPEG1); // compress
                    page->FitFile1.clear();
                }
                processedpage++;
                emit prepareBufferProgressChanged((processedpage * 100) / readypagecount);
            }
        }
        emit prepareBufferFinished();
    } else {
        int cpucount = getCpuCount();
        if (cpucount > 4) {
            cpucount--;
        }
        prepareGammaTable(120); // Optimize for changeGamma
        for (int i = 0; i < cpucount; i++) {
            ScalerThread* scaler = new ScalerThread(fChapters, fChapterMute, fScaleFilter, fGammaCorrect, fSharpen, fDitherMode, GlobalSetting.AutoCrop, GlobalSetting.AutoExposure, GlobalSetting.UseSurfaceBlur, GlobalSetting.RemoveJPEGArtifacts, fStageWidth, fStageHeight);
            QObject::connect(scaler, SIGNAL(startScale(int, int)),
                this, SLOT(onPageStarted(int, int)), Qt::QueuedConnection);
            QObject::connect(scaler, SIGNAL(finishScale(int, int)),
                this, SLOT(onPageFinished(int, int)), Qt::QueuedConnection);
            scaler->start(QThread::InheritPriority);
        }

    }

    return result;
}

void TNekoDriver::onPageStarted( int chapterindex, int pageindex )
{
    if (chapterindex < 0 || chapterindex >= fChapters.size()) {
        return;
    }
    emit prepareBufferStageChanged(fChapters[chapterindex].Chaptername);
}

void TNekoDriver::SaveBundleRec( TPageBundleRec& page )
{
    // TODO: Dual page?
    if (page.FitImage1 && page.Corrupted == false) {
        QBuffer vfile;
        vfile.setBuffer(&page.FitFile1);
        vfile.open(QIODevice::WriteOnly);
        page.FitImage1->save(&vfile, fJpegMode?"jpg":"png", fCodecQuality);
        page.FitJPEG1 = fJpegMode;
        vfile.close();
    }
    if (page.FitImage2 && page.Corrupted == false) {
        QBuffer vfile;
        vfile.setBuffer(&page.FitFile2);
        vfile.open(QIODevice::WriteOnly);
        page.FitImage2->save(&vfile, fJpegMode?"jpg":"png", fCodecQuality);
        page.FitJPEG2 = fJpegMode;
        vfile.close();
    }
    page.DropImageStore();
}

void TNekoDriver::onPageFinished( int chapterindex, int pageindex )
{
    fFinishPageCount++;
    emit prepareBufferProgressChanged((fFinishPageCount * 100) / fBusyPageCount);

    if (chapterindex >= 0 && chapterindex < fChapters.size()) {
        if (pageindex >= 0 && pageindex < fChapters[chapterindex].Pages.size()) {
            TPageBundleRec& page = fChapters[chapterindex].Pages[pageindex];
            if ((page.FitImage1 || page.FitImage2) && page.Corrupted == false) {
                SaveBundleRec(page);
            }
        }
    }

    if (fFinishPageCount == fBusyPageCount) {

        int allpagecount = 0;
        for(ChapterBundleList::iterator chapter = fChapters.begin(); chapter != fChapters.end(); chapter++) {
            bool hited = true;
            for(PageBundleList::iterator page = chapter->Pages.begin(); page != chapter->Pages.end(); page++) {
                if ((page->FitImage1 || page->FitFile1.isEmpty() == false) && page->Corrupted == false) {
                    allpagecount++;
                }
                if ((page->FitImage2 || page->FitFile2.isEmpty() == false) && page->Corrupted == false) {
                    allpagecount++;
                }
            }
        }

        emit prepareBufferStageChanged(tr("Encoding image..."));
        int addedpage = 0;
        for(ChapterBundleList::iterator chapter = fChapters.begin(); chapter != fChapters.end(); chapter++) {
            chapter->NewChapterIndex = fBookDepository.AddChapter(chapter->Chaptername);
            for(PageBundleList::iterator page = chapter->Pages.begin(); page != chapter->Pages.end(); page++) {
                if ((page->FitImage1 || page->FitImage2) && page->Corrupted == false) {
                    // never go here
                    SaveBundleRec(*page);
                }

                if (page->FitFile1.isEmpty() == false) {
                    fBookDepository.AddPageIntoChapter(chapter->NewChapterIndex, page->FitFile1, page->FitJPEG1); // compress
                    page->FitFile1.clear(); // TODO: cached
                    addedpage++;
                    emit prepareBufferProgressChanged((addedpage * 100) / allpagecount);
                }
                if (page->FitFile2.isEmpty() == false) {
                    fBookDepository.AddPageIntoChapter(chapter->NewChapterIndex, page->FitFile2, page->FitJPEG2); // compress
                    page->FitFile2.clear(); // TODO: cached
                    addedpage++;
                    emit prepareBufferProgressChanged((addedpage * 100) / allpagecount);
                }
            }
        }

        // ok?
        emit prepareBufferFinished();
    }
}