#include "SettingsUnt.h"
#include "ui_SettingsFrm.h"

TSettingsFrm::TSettingsFrm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TSettingsFrm)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
}

TSettingsFrm::~TSettingsFrm()
{
    delete ui;
}

void TSettingsFrm::StoreGlobalInfo( const TGlobalRecItem& info )
{
    fGlobalSettings = info;
    // Load info to control
    ui->sizeCombo->setCurrentIndex(fGlobalSettings.OutputResolution);
    ui->codecCombo->setCurrentIndex(fGlobalSettings.OutputFormat);
    ui->filterCombo->setCurrentIndex(fGlobalSettings.EnhanceLevel);

    ui->sortingCombo->setCurrentIndex(fGlobalSettings.PageSorting);
    if (fGlobalSettings.SplitPage == mdpmSingle) {
        ui->splitCombo->setCurrentIndex(0);
        ui->smartSplitChk->setChecked(false);
    } else {
        ui->smartSplitChk->setChecked(fGlobalSettings.SplitPage == mdpmSmartNippon ||fGlobalSettings.SplitPage == mdpmSmartNative);
        if (fGlobalSettings.SplitPage == mdpmAutoNippon || fGlobalSettings.SplitPage == mdpmSmartNippon) {
            ui->splitCombo->setCurrentIndex(1);
        } else {
            ui->splitCombo->setCurrentIndex(2);
        }
    }
    ui->exposureChk->setChecked(fGlobalSettings.AutoExposure);
    ui->artifactsChk->setChecked(fGlobalSettings.RemoveJPEGArtifacts);
    ui->cropChk->setChecked(fGlobalSettings.AutoCrop);
    ui->surfaceBlurChk->setChecked(fGlobalSettings.UseSurfaceBlur);
}

TGlobalRecItem TSettingsFrm::GetGlobalInfo()
{
    // Save into fGlobalSettings
    fGlobalSettings.OutputResolution = ResizeMode(ui->sizeCombo->currentIndex());
    fGlobalSettings.OutputFormat = CodecMode(ui->codecCombo->currentIndex());
    fGlobalSettings.EnhanceLevel = FilterMode(ui->filterCombo->currentIndex());

    fGlobalSettings.PageSorting = PageSortingMethod(ui->sortingCombo->currentIndex());
    if (ui->splitCombo->currentIndex() == 0) {
        fGlobalSettings.SplitPage = mdpmSingle;
    } else {
        if (ui->splitCombo->currentIndex() == 1) {
            fGlobalSettings.SplitPage = ui->smartSplitChk->isChecked()?mdpmSmartNippon:mdpmAutoNippon;
        } else {
            fGlobalSettings.SplitPage = ui->smartSplitChk->isChecked()?mdpmSmartNative:mdpmAutoNative;
        }
    }
    fGlobalSettings.AutoExposure = ui->exposureChk->isChecked();
    fGlobalSettings.RemoveJPEGArtifacts = ui->artifactsChk->isChecked();
    fGlobalSettings.AutoCrop = ui->cropChk->isChecked();
    fGlobalSettings.UseSurfaceBlur = ui->surfaceBlurChk->isChecked();

    return fGlobalSettings;

}

void TSettingsFrm::onPageSplitValueChanged( int newindex )
{
    if (newindex == 0) {
        ui->smartSplitChk->setChecked(false);
        ui->smartSplitChk->setEnabled(false);
    } else {
        ui->smartSplitChk->setEnabled(true);
    }
}