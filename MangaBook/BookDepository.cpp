#include "BookDepository.h"
#include <cstdio>
#include <QImage>
#include <QtXml>
#include "OpenBook/OpenBook.h"
#include "AddonFuncUnt.h"

using namespace std;

int __cdecl BookDepository::logprintf( const char * _Format, ... )
{
    va_list ext;

    va_start( ext, _Format );
    //vprintf(_Format, marker);
#ifdef __GNUC__
    int len = vsnprintf(NULL, 0, _Format, ext);
    // FIXME: differnt than a _vsnprintf one
#else
    int len = _vscprintf(_Format, ext);
#endif
    QByteArray buf;
    buf.resize(len + 1);
    buf.fill(0);
#ifdef __GNUC__
    len = vsnprintf(buf.data(), len, _Format, ext);
#else
    len = _vsnprintf_s(buf.data(), buf.size(), len, _Format, ext);
#endif
    va_end(ext);

    AddLog(buf, ltMessage);

    return len;
}

void BookDepository::AddLog( QString content, TLogType logtype )
{
    emit logStored(content, logtype);
}

BookDepository::BookDepository()
{

}

int BookDepository::AddChapter( const QString& Title )
{
    int result = fTopicRec.Chapters.size();
    TChapterRecItem item;
    item.ChapterTitle = Title;
    item.HideTitle = true;
    fTopicRec.Chapters.push_back(item);
    return result;
}

bool BookDepository::AddPageIntoChapter( int index, const QByteArray& content, bool jpeg )
{
    bool result = false;
    if (index < 0 || index >= fTopicRec.Chapters.size()) {
        return result;
    }
    
    int pagecount = fTopicRec.Chapters[index].Pages.size();
    TPageRecItem item;
    item.ImageVPath = QString("c%1p%2.%3").arg(index + 1).arg(pagecount + 1).arg(jpeg?"jpg":"png");
    //QBuffer vfile;
    //vfile.setBuffer(&item.FileContent);
    //vfile.open(QIODevice::WriteOnly);
    //image->save(&vfile ,fJpegMode?"jpg":"png", fCodecQuality);
    //vfile.close();
    item.FileContent = content;
    fTopicRec.Chapters[index].Pages.push_back(item);

    result = true;
    return result;
}

void BookDepository::SetBookInfo( const QString& Title, const QString& Author, const QString& Brief )
{
    fBookRec.BookTitle = Title;
    fBookRec.Author = Author;
    fBookRec.Brief = Brief;
}

void BookDepository::SetCover( QImage* image )
{
    fBookRec.Cover.ImageVPath = "cover.jpg";

    QBuffer vfile;
    vfile.setBuffer(&fBookRec.Cover.FileContent);
    vfile.open(QIODevice::WriteOnly);
    image->save(&vfile ,"jpg", 80);
    vfile.close();
}

bool BookDepository::GenerateBookFromBuffer( const QString& snbfilename )
{
    // Prepare all xml
    bool result = false;
    
    // book.snbf
    QBuffer vfile;
    vfile.setBuffer(&fBookRec.XmlContent);
    vfile.open(QIODevice::WriteOnly);
    QXmlStreamWriter writer(&vfile);

    writer.setAutoFormatting(true);
    writer.setAutoFormattingIndent(0);
    writer.setCodec("UTF-8");

    writer.writeStartDocument("1.0"); // <?xml version="1.0" encoding="UTF-8"?>

    writer.writeStartElement("book-snbf");
    writer.writeAttribute("version", "1.0");
    writer.writeStartElement("head");
    writer.writeTextElement("name", fBookRec.BookTitle);
    writer.writeTextElement("author", fBookRec.Author);
    writer.writeTextElement("language", "ZH-CN");
    writer.writeEmptyElement("rights");
    writer.writeEmptyElement("publisher");
    writer.writeTextElement("generator", "MangaBook");
    writer.writeEmptyElement("created");
    writer.writeTextElement("abstract", fBookRec.Brief);
    writer.writeTextElement("cover", fBookRec.Cover.ImageVPath);
    writer.writeEndElement(); // head
    writer.writeEndElement(); // book-snbf

    writer.writeEndDocument();

    vfile.close();

    // toc.snbf
    vfile.setBuffer(&fTopicRec.XmlContent);
    vfile.open(QIODevice::WriteOnly);
    writer.setDevice(&vfile);

    writer.writeStartDocument("1.0"); // <?xml version="1.0" encoding="UTF-8"?>

    writer.writeStartElement("toc-snbf");
    writer.writeStartElement("head");
    writer.writeTextElement("chapters", QString("%1").arg(fTopicRec.Chapters.size()));
    writer.writeEndElement(); // head
    writer.writeStartElement("body");
    int chapterindex = 0;
    foreach (TChapterRecItem chapter, fTopicRec.Chapters) {
        writer.writeStartElement("chapter");
        writer.writeAttribute("src", QString("c%1.snbc").arg(chapterindex + 1));
        writer.writeCDATA(chapter.ChapterTitle);
        writer.writeEndElement(); // chapter
        chapterindex++;
    }
    writer.writeEndElement(); // body
    writer.writeEndElement(); // toc-snbf

    writer.writeEndDocument();

    vfile.close();

    int allfiles = 2; // book toc
    // c1.snbc
    for (QVector < TChapterRecItem >::iterator chapter = fTopicRec.Chapters.begin(); chapter != fTopicRec.Chapters.end(); chapter++) {
        allfiles++;
        vfile.setBuffer(&chapter->XmlContent);
        vfile.open(QIODevice::WriteOnly);
        writer.setDevice(&vfile);

        writer.writeStartDocument("1.0"); // <?xml version="1.0" encoding="UTF-8"?>

        writer.writeStartElement("snbc");
        writer.writeStartElement("head");
        //writer.writeTextElement("title", chapter->ChapterTitle);
        writer.writeStartElement("title");
        writer.writeCDATA(chapter->ChapterTitle);
        writer.writeEndElement(); // title
        writer.writeTextElement("hidetitle", chapter->HideTitle?"true":"false");
        writer.writeEndElement(); // head
        writer.writeStartElement("body");
        foreach (TPageRecItem page, chapter->Pages) {
            allfiles++;
            writer.writeTextElement("img", page.ImageVPath);
        }
        writer.writeEndElement(); // body
        writer.writeEndElement(); // snbc

        writer.writeEndDocument();

        vfile.close();
    }

#ifdef _DEBUG
    QString dumpdir = QString("%1/%2Dump").arg(QFileInfo(snbfilename).path()).arg(QFileInfo(snbfilename).baseName());
#endif

    int insertedfile = 0;
    QFile file(snbfilename);
    file.open(QIODevice::WriteOnly);
    OpenBookWriter bookwriter;
    // CloundLibrary client has strange check codes.
    // you must put files in this order: chapter image book toc
    // bambook reader have more strange bug, you must store all files in snb by alpha sort order
    QList < TDummyFileRec > readerworkarounder;
    chapterindex = 0;
    foreach (TChapterRecItem chapter, fTopicRec.Chapters) {
        if (chapter.XmlContent.isEmpty() == false) {
            insertedfile++;
            emit snbProgressChanged((insertedfile * 100) / allfiles);
            bookwriter.AddPlainFile(QString("snbc/c%1.snbc").arg(chapterindex + 1), chapter.XmlContent);
#ifdef _DEBUG
            SaveByteArrayToFile(QString("%1/snbc/c%2.snbc").arg(dumpdir).arg(chapterindex + 1), chapter.XmlContent);
#endif
        }
        chapterindex++;
    }
    foreach (TChapterRecItem chapter, fTopicRec.Chapters) {
        foreach (TPageRecItem page, chapter.Pages) {
            insertedfile++;
            emit snbProgressChanged((insertedfile * 100) / allfiles);
            bookwriter.AddBinaryFile(QString("snbc/images/%1").arg(page.ImageVPath), page.FileContent);
#ifdef _DEBUG
            SaveByteArrayToFile(QString("%1/snbc/images/%2").arg(dumpdir).arg(page.ImageVPath), page.FileContent);
#endif
        }
    }
    if (fBookRec.Cover.ImageVPath.isEmpty() == false && fBookRec.Cover.FileContent.isEmpty() == false) {
        insertedfile++;
        emit snbProgressChanged((insertedfile * 100) / allfiles);
        bookwriter.AddBinaryFile(QString("snbc/images/%1").arg(fBookRec.Cover.ImageVPath), fBookRec.Cover.FileContent);
#ifdef _DEBUG
        SaveByteArrayToFile(QString("%1/snbc/images/%2").arg(dumpdir).arg(fBookRec.Cover.ImageVPath), fBookRec.Cover.FileContent);
#endif
    }

    if (fBookRec.XmlContent.isEmpty() == false) {
        insertedfile++;
        emit snbProgressChanged(insertedfile * 100 / allfiles);
        bookwriter.AddPlainFile("snbf/book.snbf", fBookRec.XmlContent);
#ifdef _DEBUG
        SaveByteArrayToFile(dumpdir + "/snbf/book.snbf", fBookRec.XmlContent);
#endif
    }
    if (fTopicRec.XmlContent.isEmpty() == false) {
        insertedfile++;
        emit snbProgressChanged((insertedfile * 100) / allfiles);
        bookwriter.AddPlainFile("snbf/toc.snbf", fTopicRec.XmlContent);
#ifdef _DEBUG
        SaveByteArrayToFile(dumpdir + "/snbf/toc.snbf", fTopicRec.XmlContent);
#endif
    }

    bookwriter.OutputSndaBook(&file);
    if (file.isOpen()) {
        file.close();
    }

    result = true;

    return result;
}

bool BookDepository::CleanupAllBuffers()
{
    fBookRec.Cover.ImageVPath.clear();
    fBookRec.Cover.FileContent.clear();
    fBookRec.XmlContent.clear();
    if (fTopicRec.Chapters.isEmpty()) {
        return false;
    }
    fTopicRec.Chapters.clear();
    fTopicRec.XmlContent.clear();
    return true;
}

bool RemoveXmlDocumentMark( QByteArray& xmlcontent )
{
    // remove xml version line.
    if (xmlcontent.isEmpty() == false) {
        QBuffer truncater(&xmlcontent);
        truncater.open(QIODevice::ReadOnly);
        truncater.readLine();
        QByteArray newcontent = truncater.readAll();
        truncater.close();
        xmlcontent = newcontent;
        return true;
    }
    return false;
}