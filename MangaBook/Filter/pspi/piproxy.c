/* piproxy -- Photoshop plug-in debugging proxy
 *
 * Copyright (C) 2001 Tor Lillqvist
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#define STRICT
#include <windows.h>
#undef STRICT

#include <glib.h>

#define WINDOWS         /* PSSDK headers want this */
#include <PIGeneral.h>
#include <PIAbout.h>
#include <PIFilter.h>
#include <PIUtilities.h>
#include <WinUtilities.h>

#ifndef ENTRYPOINT
#error You should define ENTRYPOINT when compiling piproxy.c
#endif

#define STRINGIFY(x) STRINGIFY2(x)
#define STRINGIFY2(x) #x

#define RECT_NONEMPTY(r) (r.left < r.right && r.top < r.bottom)
#define PRINT_RECT(r) fprintf (log, "%d:%dx%d:%d", r.left, r.right, r.top, r.bottom) 

static FILE *log = NULL;

static BufferProcs *pBufferProcs;
static ChannelPortProcs *pChannelPortProcs;
static PIDescriptorParameters *pPIDescriptorParameters;
static HandleProcs *pHandleProcs;
static ImageServicesProcs *pImageServicesProcs;
static PropertyProcs *pPropertyProcs;
static ResourceProcs *pResourceProcs;
static SPBasicSuite *pSPBasicSuite;

/* Due to the strange design of the Photoshop API (no user data in
 * callbacks) we must keep state in global variables. Blecch. */

static PlatformData *platform;
static FilterRecord *host_filter = NULL, *orig_filter = NULL;
static int32 *data_handle;
static HMODULE plugin_dll = NULL;

static void (CALLBACK *plugin_ep)(const int16   selector,
				  FilterRecord *filterRecord,
				  int32        *data,
				  int16        *result);

static char *
int32_as_be_4c (int32 i)
{
  const char *cp = (const char *) &i;
  static char buf[5*10];
  static int bufindex = 0;
  
  char *bufp = buf + bufindex;

  bufp[0] = cp[3];
  bufp[1] = cp[2];
  bufp[2] = cp[1];
  bufp[3] = cp[0];
  bufp[4] = '\0';

  bufindex += 5;
  if (bufindex == sizeof(buf))
    bufindex = 0;
  
  return bufp;
}

static char *
boolean_string (Boolean value)
{
  return (value ? "TRUE" : "FALSE");
}

static gchar *
error_string (int16 result)
{
  int msglen;
  static gchar s[1000];
  gchar tem[1000];

  switch (result)
    {
#define CASE(x) case x: sprintf (s, "%d (" #x ")", result); return s;
    CASE (noErr);
    CASE (userCanceledErr);
    CASE (readErr);
    CASE (writErr);
    CASE (openErr);
    CASE (dskFulErr);
    CASE (ioErr);
    CASE (memFullErr);
    CASE (nilHandleErr);
    CASE (filterBadParameters);
    CASE (filterBadMode);
    CASE (errPlugInHostInsufficient);
    CASE (errPlugInPropertyUndefined);
    CASE (errHostDoesNotSupportColStep);
    CASE (errInvalidSamplePoint);
#undef CASE
    case errReportString:
      msglen = (*host_filter->errorString)[0];
      strncpy (tem, (*host_filter->errorString)+1, sizeof (tem));
      tem[sizeof (tem) - 1] = '\0';
      sprintf (s, "%d (%s)", result, tem);
      return s;
    default:
      sprintf (s, "%d (other error)", result);
      return s;
    }
}

static char *
image_mode_string (int image_mode)
{
  static char s[100];

  switch (image_mode)
    {
#define CASE(x) case x: return #x + strlen ("plugInMode");
    CASE (plugInModeBitmap);
    CASE (plugInModeGrayScale);
    CASE (plugInModeIndexedColor);
    CASE (plugInModeRGBColor);
    CASE (plugInModeCMYKColor);
    CASE (plugInModeHSLColor);
    CASE (plugInModeHSBColor);
    CASE (plugInModeMultichannel);
    CASE (plugInModeDuotone);
    CASE (plugInModeLabColor);
    CASE (plugInModeGray16);
    CASE (plugInModeRGB48);
    CASE (plugInModeLab48);
    CASE (plugInModeCMYK64);
    CASE (plugInModeDeepMultichannel);
    CASE (plugInModeDuotone16);
#undef CASE
    default:
      sprintf (s, "plugInMode???(%d)", image_mode);
      return s;
    }
}

static char *
layout_string (int layout)
{
  static char s[100];

  switch (layout)
    {
#define CASE(x) case x: return #x + strlen ("piLayout");
    CASE (piLayoutRowsColumnsPlanes);
    CASE (piLayoutRowsPlanesColumns);
    CASE (piLayoutColumnsRowsPlanes);
    CASE (piLayoutColumnsPlanesRows);
    CASE (piLayoutPlanesRowsColumns);
    CASE (piLayoutPlanesColumnsRows);
#undef CASE
    default:
      sprintf (s, "piLayout???(%d)", layout);
      return s;
    }
}

static char *
color_services_string (int selector)
{
  static char s[100];

  switch (selector)
    {
#define CASE(x) case x: return #x + strlen ("plugInColorServices");
    CASE (plugIncolorServicesChooseColor);
    CASE (plugIncolorServicesConvertColor);
    CASE (plugIncolorServicesSamplePoint);
    CASE (plugIncolorServicesGetSpecialColor);
#undef CASE
    default:
      sprintf (s, "plugIncolorServices???(%d)", selector);
      return s;
    }
}

static char *
vrect_string (const VRect *rect)
{
  static char s[100];

  sprintf (s, "%ld:%ldx%ld:%ld", rect->left, rect->right, rect->top, rect->bottom);
  return s;
}

static char *
pspixelmap_string (const PSPixelMap *source)
{
  static char s[1000];

  sprintf (s, "bounds=%s rowBytes=%d colBytes=%d planeBytes=%d",
	   vrect_string (&source->bounds), source->rowBytes, source->colBytes, source->planeBytes);
  return s;
}

#include "proxies.c"

static OSErr
advanceState (void)
{
  OSErr result;
  gchar *e;

  fprintf (log, "advanceState in:");
  PRINT_RECT (host_filter->inRect);
  fprintf (log, ",%d:%d", host_filter->inLoPlane, host_filter->inHiPlane);
  fprintf (log, " out:");
  PRINT_RECT (host_filter->outRect);
  fprintf (log, ",%d:%d", host_filter->outLoPlane, host_filter->outHiPlane);
  fprintf (log, " mask:");
  PRINT_RECT (host_filter->maskRect);
  fprintf (log, "\n  inData=%p inRowBytes=%d outData=%p outRowBytes=%d\n"
	   "  maskData=%p maskRowBytes=%d\n",
	   host_filter->inData, host_filter->inRowBytes,
	   host_filter->outData, host_filter->outRowBytes,
	   host_filter->maskData, host_filter->maskRowBytes); 
  result = orig_filter->advanceState ();
  e = error_string (result);
  fprintf (log, "  : %s\n  inData=%p inRowBytes=%d outData=%p outRowBytes=%d\n"
	   "  maskData=%p maskRowBytes=%d\n",
	   e,
	   host_filter->inData, host_filter->inRowBytes,
	   host_filter->outData, host_filter->outRowBytes,
	   host_filter->maskData, host_filter->maskRowBytes); 
  g_free (e);
  return result;
}

static void
print_point (const Point *p)
{
  fprintf (log, "%d,%d", p->h, p->v);
}

static void
print_rect (const Rect *r)
{
  fprintf (log, "%d:%dx%d:%d", r->left, r->right, r->top, r->bottom);
}

static void
print_rgb (const RGBColor *c)
{
  fprintf (log, "%04x,%04x,%04x", c->red, c->green, c->blue);
}

static void
print_filter_color (const FilterColor *c)
{
  fprintf (log, "%02x,%02x,%02x", c[0], c[1], c[2]);
}

static void
print_filter_record (const FilterRecord *filter)
{
  fprintf (log, "  serialNumber:%ld\n", filter->serialNumber);
#if 0
  fprintf (log, "  abortProc:%p\n", filter->abortProc);
  fprintf (log, "  progressProc:%p\n", filter->progressProc);
#endif
  fprintf (log, "  parameters:%p\n", filter->parameters);
  fprintf (log, "  imageSize:");
  print_point (&filter->imageSize);
  fprintf (log, "\n");
  fprintf (log, "  planes:%d\n", filter->planes);
  fprintf (log, "  filterRect:");
  print_rect (&filter->filterRect);
  fprintf (log, "\n");
  fprintf (log, "  background:");
  print_rgb (&filter->background);
  fprintf (log, "\n");
  fprintf (log, "  foreground:");
  print_rgb (&filter->foreground);
  fprintf (log, "\n");
  fprintf (log, "  maxSpace:%ld\n", filter->maxSpace);
  fprintf (log, "  bufferSpace:%ld\n", filter->bufferSpace);
  fprintf (log, "  inRect:");
  print_rect (&filter->inRect);
  fprintf (log, "\n");
  fprintf (log, "  inLoPlane:%d\n", filter->inLoPlane);
  fprintf (log, "  inHiPlane:%d\n", filter->inHiPlane);
  fprintf (log, "  outRect:");
  print_rect (&filter->outRect);
  fprintf (log, "\n");
  fprintf (log, "  outLoPlane:%d\n", filter->outLoPlane);
  fprintf (log, "  outHiPlane:%d\n", filter->outHiPlane);
  fprintf (log, "  inData:%p\n", filter->inData);
  fprintf (log, "  inRowBytes:%d\n", filter->inRowBytes);
  fprintf (log, "  outData:%p\n", filter->outData);
  fprintf (log, "  outRowBytes:%d\n", filter->outRowBytes);
  fprintf (log, "  isFloating:%d\n", filter->isFloating);
  fprintf (log, "  haveMask:%d\n", filter->haveMask);
  fprintf (log, "  autoMask:%d\n", filter->autoMask);
  fprintf (log, "  maskRect:");
  print_rect (&filter->maskRect);
  fprintf (log, "\n");
  fprintf (log, "  maskData:%p\n", filter->maskData);
  fprintf (log, "  maskRowBytes:%d\n", filter->maskRowBytes);
  fprintf (log, "  backColor:");
  print_filter_color (&filter->backColor);
  fprintf (log, "\n");
  fprintf (log, "  foreColor:");
  print_filter_color (&filter->foreColor);
  fprintf (log, "\n");
  fprintf (log, "  hostSig:%s\n", int32_as_be_4c (filter->hostSig));
#if 0
  fprintf (log, "  hostProc:%p\n", filter->hostProc);
#endif
  fprintf (log, "  imageMode:%s\n", image_mode_string (filter->imageMode));
  fprintf (log, "  imageHRes:%f\n", filter->imageHRes / (double) 0x10000);
  fprintf (log, "  imageVRes:%f\n", filter->imageVRes / (double) 0x10000);
  fprintf (log, "  floatCoord:");
  print_point(&filter->floatCoord);
  fprintf (log, "\n");
  fprintf (log, "  wholeSize:");
  print_point (&filter->wholeSize);
  fprintf (log, "\n");
  fprintf (log, "  monitor:%p\n", filter->monitor);
  fprintf (log, "  platformData:%p\n", filter->platformData);
#if 0
  fprintf (log, "  bufferProcs:%p\n", filter->bufferProcs);
  fprintf (log, "  resourceProcs:%p\n", filter->resourceProcs);
  fprintf (log, "  processEvent:%p\n", filter->processEvent);
  fprintf (log, "  displayPixels:%p\n", filter->displayPixels);
  fprintf (log, "  handleProcs:%p\n", filter->handleProcs);
#endif
  fprintf (log, "  supportsDummyChannels:%d\n", filter->supportsDummyChannels);
  fprintf (log, "  supportsAlternateLayouts:%d\n", filter->supportsAlternateLayouts);
  fprintf (log, "  wantLayout:%s\n", layout_string (filter->wantLayout));
  fprintf (log, "  filterCase:%d\n", filter->filterCase);
  fprintf (log, "  dummyPlaneValue:%d\n", filter->dummyPlaneValue);
#if 0
  fprintf (log, "  premiereHook:%d\n", filter->premiereHook);
  fprintf (log, "  advanceState:%p\n", filter->advanceState);
#endif
  fprintf (log, "  supportsAbsolute:%d\n", filter->supportsAbsolute);
  fprintf (log, "  wantsAbsolute:%d\n", filter->wantsAbsolute);
#if 0
  fprintf (log, "  getPropertyObsolete:%p\n", filter->getPropertyObsolete);
#endif
  fprintf (log, "  cannotUndo:%d\n", filter->cannotUndo);
  fprintf (log, "  supportsPadding:%d\n", filter->supportsPadding);
  fprintf (log, "  inputPadding:%d\n", filter->inputPadding);
  fprintf (log, "  outputPadding:%d\n", filter->outputPadding);
  fprintf (log, "  maskPadding:%d\n", filter->maskPadding);
  fprintf (log, "  samplingSupport:%d\n", filter->samplingSupport);
  fprintf (log, "  inputRate:%f\n", filter->inputRate / (double) 0x10000);
  fprintf (log, "  maskRate:%f\n", filter->maskRate / (double) 0x10000);
#if 0
  fprintf (log, "  colorServices:%p\n", filter->colorServices);
#endif
  fprintf (log, "  inLayerPlanes:%d\n", filter->inLayerPlanes);
  fprintf (log, "  inTransparencyMask:%d\n", filter->inTransparencyMask);
  fprintf (log, "  inLayerMasks:%d\n", filter->inLayerMasks);
  fprintf (log, "  inInvertedLayerMasks:%d\n", filter->inInvertedLayerMasks);
  fprintf (log, "  inNonLayerPlanes:%d\n", filter->inNonLayerPlanes);
  fprintf (log, "  outLayerPlanes:%d\n", filter->outLayerPlanes);
  fprintf (log, "  outTransparencyMask:%d\n", filter->outTransparencyMask);
  fprintf (log, "  outLayerMasks:%d\n", filter->outLayerMasks);
  fprintf (log, "  outInvertedLayerMasks:%d\n", filter->outInvertedLayerMasks);
  fprintf (log, "  outNonLayerPlanes:%d\n", filter->outNonLayerPlanes);
  fprintf (log, "  absLayerPlanes:%d\n", filter->absLayerPlanes);
  fprintf (log, "  absTransparencyMask:%d\n", filter->absTransparencyMask);
  fprintf (log, "  absLayerMasks:%d\n", filter->absLayerMasks);
  fprintf (log, "  absInvertedLayerMasks:%d\n", filter->absInvertedLayerMasks);
  fprintf (log, "  absNonLayerPlanes:%d\n", filter->absNonLayerPlanes);
  fprintf (log, "  inPreDummyPlanes:%d\n", filter->inPreDummyPlanes);
  fprintf (log, "  inPostDummyPlanes:%d\n", filter->inPostDummyPlanes);
  fprintf (log, "  outPreDummyPlanes:%d\n", filter->outPreDummyPlanes);
  fprintf (log, "  outPostDummyPlanes:%d\n", filter->outPostDummyPlanes);
  fprintf (log, "  inColumnBytes:%d\n", filter->inColumnBytes);
  fprintf (log, "  inPlaneBytes:%d\n", filter->inPlaneBytes);
  fprintf (log, "  outColumnBytes:%d\n", filter->outColumnBytes);
  fprintf (log, "  outPlaneBytes:%d\n", filter->outPlaneBytes);
#if 0
  fprintf (log, "  imageServicesProcs:%p\n", filter->imageServicesProcs);
  fprintf (log, "  propertyProcs:%p\n", filter->propertyProcs);
#endif
  fprintf (log, "  inTileHeight:%d\n", filter->inTileHeight);
  fprintf (log, "  inTileWidth:%d\n", filter->inTileWidth);
  fprintf (log, "  inTileOrigin:%d\n", filter->inTileOrigin);
  fprintf (log, "  absTileHeight:%d\n", filter->absTileHeight);
  fprintf (log, "  absTileWidth:%d\n", filter->absTileWidth);
  fprintf (log, "  absTileOrigin:");
  print_point (&filter->absTileOrigin);
  fprintf (log, "\n");
  fprintf (log, "  outTileHeight:%d\n", filter->outTileHeight);
  fprintf (log, "  outTileWidth:%d\n", filter->outTileWidth);
  fprintf (log, "  outTileOrigin:");
  print_point (&filter->outTileOrigin);
  fprintf (log, "\n");
  fprintf (log, "  maskTileHeight:%d\n", filter->maskTileHeight);
  fprintf (log, "  maskTileWidth:%d\n", filter->maskTileWidth);
  fprintf (log, "  maskTileOrigin:");
  print_point (&filter->maskTileOrigin);
  fprintf (log, "\n");
  fprintf (log, "  descriptorParameters:%p\n", filter->descriptorParameters);
#if 0
  fprintf (log, "  errorString:%.*s\n", (*filter->errorString)[0], (*filter->errorString)+1);
  fprintf (log, "  channelPortProcs:%p\n", filter->channelPortProcs);
  fprintf (log, "  documentInfo:%p\n", filter->documentInfo);
  fprintf (log, "  sSPBasic:%p\n", filter->sSPBasic);
#endif
  fprintf (log, "  plugInRef:%p\n", filter->plugInRef);
  fprintf (log, "  depth:%d\n", filter->depth);
#if 0
  fprintf (log, "  iCCprofileData:%p\n", filter->iCCprofileData);
  fprintf (log, "  iCCprofileSize:%d\n", filter->iCCprofileSize);
  fprintf (log, "  canUseICCProfiles:%d\n", filter->canUseICCProfiles);
#endif
}

static void
setup_suites (void)
{
  if (pBufferProcs != NULL)
    return;

  pBufferProcs = g_new (BufferProcs, 1);
  pChannelPortProcs = g_new (ChannelPortProcs, 1);
  pPIDescriptorParameters = g_new (PIDescriptorParameters, 1);
  pHandleProcs = g_new (HandleProcs, 1);
  pImageServicesProcs = g_new (ImageServicesProcs, 1);
  pPropertyProcs = g_new (PropertyProcs, 1);
  pResourceProcs = g_new (ResourceProcs, 1);
  pSPBasicSuite = g_new (SPBasicSuite, 1);

  SUITEINITS();
}

static int16
load_plugin ()
{
  gchar *dll_name;
  gchar *entrypoint_name = STRINGIFY(ENTRYPOINT);

  if (plugin_dll != NULL)
    return noErr;

  dll_name = getenv ("PIPROXY_TARGET");
  if (dll_name == NULL)
    {
      fprintf (log, "No PIPROXY_TARGET environment variable");
      return userCanceledErr;
    }

  fprintf (log, "dll=%s entrypoint=%s\n", dll_name, entrypoint_name);

  if ((plugin_dll = LoadLibrary (dll_name)) == NULL)
    {
      fprintf (log, "LoadLibrary (%s) failed: %s",
           dll_name,
           g_win32_error_message (GetLastError ()));
      return userCanceledErr;
    }
  
  if ((plugin_ep = GetProcAddress (plugin_dll, entrypoint_name))== NULL)
    {
      fprintf (log, "GetProcAddress (%s, %s) failed: %s\n",
           dll_name, entrypoint_name,
           g_win32_error_message (GetLastError ()));
      FreeLibrary (plugin_dll);
      return userCanceledErr;
    }
  return noErr;
}

__declspec (dllexport) void
ENTRYPOINT (const int16   selector,
	    FilterRecord *filterRecord,
	    int32        *data,
	    int16        *result)
{
  gchar *log_name;

  if (log == NULL)
    {
      log_name = getenv ("PIPROXY_LOG");
      if (log_name == NULL)
	log_name = "piproxy.log";
      log = fopen (log_name, "a");
      if (log == NULL)
	{
	  MessageBox (NULL, "Could not open log file", "piproxy", 0);
	  *result = userCanceledErr;
	  return;
	}
      setvbuf (log, NULL, _IONBF, BUFSIZ);
      fprintf (log, "sizeof (FilterRecord)=%d\n", sizeof (FilterRecord));
    }

  setup_suites ();

  switch (selector)
    {
#define CASE(x) case x: fprintf (log, #x + strlen ("filterSelector")); break
    CASE (filterSelectorParameters);
    CASE (filterSelectorPrepare);
    CASE (filterSelectorStart);
    CASE (filterSelectorContinue);
    CASE (filterSelectorFinish);
#undef CASE
    default:
      fprintf (log, "%d\n", selector);
      *result = userCanceledErr;
      return;
    }
  fprintf (log, "\n");
  fprintf (log, " filterRecord:%p\n", filterRecord);
  print_filter_record (filterRecord);

  host_filter = filterRecord;
  if (orig_filter == NULL)
    orig_filter = g_new (FilterRecord, 1);
  *orig_filter = *host_filter;

  host_filter->abortProc = abortProc;
  host_filter->progressProc = progressProc;
  host_filter->hostProc = hostProc;
  host_filter->colorServices = colorServices;
  host_filter->processEvent = processEvent;
  host_filter->displayPixels = displayPixels;
  host_filter->advanceState = advanceState;

  host_filter->bufferProcs = pBufferProcs;
  host_filter->channelPortProcs = pChannelPortProcs;
  host_filter->handleProcs = pHandleProcs;
  host_filter->imageServicesProcs = pImageServicesProcs;
  host_filter->propertyProcs = pPropertyProcs;
  host_filter->resourceProcs = pResourceProcs;
  host_filter->sSPBasic = pSPBasicSuite;

  fprintf (log, " data:%#lx\n", *data);

  if ((*result = load_plugin ()) != noErr)
    return;

  (*plugin_ep) (selector, filterRecord, data, result);

  fprintf (log, "result=%s\n", error_string (*result));
  fprintf (log, " data:%#lx\n", *data);
  fprintf (log, " filterRecord:\n");
  print_filter_record (filterRecord);

  host_filter->abortProc = orig_filter->abortProc;
  host_filter->progressProc = orig_filter->progressProc;
  host_filter->hostProc = orig_filter->hostProc;
  host_filter->colorServices = orig_filter->colorServices;
  host_filter->processEvent = orig_filter->processEvent;
  host_filter->displayPixels = orig_filter->displayPixels;
  host_filter->advanceState = orig_filter->advanceState;

  host_filter->bufferProcs = orig_filter->bufferProcs;
  host_filter->channelPortProcs = orig_filter->channelPortProcs;
  host_filter->handleProcs = orig_filter->handleProcs;
  host_filter->imageServicesProcs = orig_filter->imageServicesProcs;
  host_filter->propertyProcs = orig_filter->propertyProcs;
  host_filter->resourceProcs = orig_filter->resourceProcs;
  host_filter->sSPBasic = orig_filter->sSPBasic;
}
