/* 
 * Wavelet sharpen GIMP plugin
 * 
 * sharpen.c
 * Copyright 2008 by Marco Rossini
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2+
 * as published by the Free Software Foundation.
 */

#include "plugin.h"
#include <QtGui/QImage>
//#include <windows.h>

void
sharpen (QImage& qimage, double amount, double radius, bool luminanceonly)
{
    int i, width, height, x, c;
    uchar *line;
    float val[4];

    int channels = qimage.depth() / 8; // 8, 16, 24, 32

    float *fimg[4];
    float *buffer[3];

    width = qimage.width();
    height = qimage.height();

    /* allocate buffers */
    /* FIXME: replace by GIMP funcitons */
    for (i = 0; i < channels; i++)
    {
        fimg[i] = (float *) malloc (width * height
            * sizeof (float));
    }
    buffer[1] = (float *) malloc(width * height
        * sizeof (float));
    buffer[2] = (float *) malloc (width * height
        * sizeof (float));

    /* read the full image from GIMP */
    for (i = 0; i < height; i++)
    {
        line = qimage.scanLine(i);

        /* convert pixel values to float and do colour model conv. */
        for (x = 0; x < width; x++)
        {
            for (c = 0; c < channels; c++)
                val[c] = (float) line[x * channels + c];
            if (channels > 2 && luminanceonly == true)
                rgb2ycbcr (&(val[0]), &(val[1]), &(val[2]));
            /* save pixel values and scale for sharpening */
            for (c = 0; c < channels; c++) {
                fimg[c][i * width + x] = val[c] / 255.0;
            }
            /* FIXME: do gamma correction */
        }
    }

    /* sharpen the channels individually */
    /* FIXME: variable abuse (x) */
    /* FIXME: do not sharpen alpha channel */
    for (c = 0; c < ((channels % 2) ? channels : channels - 1); c++)
    {
        if (luminanceonly == true && c > 0)
            continue;
        buffer[0] = fimg[c];
        if (amount > 0)
        {
            wavelet_sharpen (buffer, width, height,
                amount,
                radius);
        }
    }

    /* retransform the image data */
    for (c = 0; c < channels; c++) {
        for (i = 0; i < width * height; i++) {
            fimg[c][i] = fimg[c][i] * 255;
        }
    }

    /* convert to RGB if necessary */
    if (channels > 2 && luminanceonly == true) {
        for (i = 0; i < width * height; i++) {
            ycbcr2rgb (&(fimg[0][i]), &(fimg[1][i]), &(fimg[2][i]));
        }
    }

    /* clip the values */
    for (c = 0; c < channels; c++)
    {
        for (i = 0; i < width * height; i++)
        {
            fimg[c][i] = CLIP (fimg[c][i], 0, 255);
        }
    }

    /* write the image back to GIMP */
    for (i = 0; i < height; i++)
    {
        line = qimage.scanLine(i);

        /* convert back to uchar */
        for (c = 0; c < channels; c++) {
            for (x = 0; x < width; x++) {
                /* avoiding rounding errors !!! */
                line[x * channels + c] = (uchar) (fimg[c][i * width + x] + 0.5);
            }
        }
    }

    /* free buffers */
    /* FIXME: replace by GIMP functions */
    for (i = 0; i < channels; i++)
    {
        free (fimg[i]);
    }
    free (buffer[1]);
    free (buffer[2]);
}
