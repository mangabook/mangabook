/* 
 * Wavelet sharpen GIMP plugin
 * 
 * plugin.h
 * Copyright 2008 by Marco Rossini
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2+
 * as published by the Free Software Foundation.
 */

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include <stdlib.h>
#include <math.h>
#include <locale.h>


#define MAX2(x,y) ((x) > (y) ? (x) : (y))
#define MIN2(x,y) ((x) < (y) ? (x) : (y))
#define CLIP(x,min,max) MAX2((min), MIN2((x), (max)))

//void sharpen (GimpDrawable * drawable, GimpPreview * preview);
#ifdef __cplusplus
extern "C" {
#endif
void wavelet_sharpen (float *fimg[3], unsigned int width,
			     unsigned int height, double amount,
			     double radius);
void rgb2ycbcr (float *r, float *g, float *b);
void ycbcr2rgb (float *y, float *cb, float *cr);
#ifdef __cplusplus
}
#endif

#endif /* __PLUGIN_H__ */
