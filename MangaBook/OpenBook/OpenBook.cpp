#include "OpenBook.h"
#include <QBuffer>
#include "../AddonFuncUnt.h"
#include "bzlib.h"

bool OpenBookWriter::AddBinaryFile( QString filename, const QByteArray& content )
{
    TDummyFileRec dummy;
    dummy.IsBinary = true;
    dummy.VPath = filename;
    dummy.Content = content;
    fUnsortFiles.append(dummy);
    
    return true;
}

bool OpenBookWriter::AddPlainFile( QString filename, const QByteArray& content )
{
    TDummyFileRec dummy;
    dummy.IsBinary = false;
    dummy.VPath = filename;
    dummy.Content = content;
    fUnsortFiles.append(dummy);

    return true;
}

bool OpenBookWriter::OutputSndaBook( QIODevice* file )
{
    bool result = false;
    
    if (file->isOpen() == false) {
        file->open(QIODevice::WriteOnly);
    }
    QDataStream writer(file);
    writer.setByteOrder(QDataStream::BigEndian);

    // rulz. you must store files in 1,10,11,12,2,3,4,5 order
    FlushUnsortBuffer();

    TVMBR vmbr;
    vmbr.FileCount = fVirtualFiles.size();
    vmbr.VFatSize = vmbr.FileCount * 12;
    QBuffer vfatsource; // use internal cache
    vfatsource.open(QIODevice::WriteOnly);
    QDataStream vfatwriter(&vfatsource);
    vfatwriter.setByteOrder(QDataStream::BigEndian);
    foreach (TVFileRec vfile, fVirtualFiles) {
        TVFatRec vfat;
        vfat.Attribute = vfile.IsBinary?0x01000000:0x41000000;
        vfat.FilenameOffset = vmbr.VFatSize - vmbr.FileCount * 12; // 0
        vfat.OriginalFileSize = vfile.Size;
        vfatwriter << vfat;
        vmbr.VFatSize += vfile.VPath.size() + 1; // str, 0
    }
    foreach (TVFileRec vfile, fVirtualFiles) {
        vfatwriter.writeRawData(vfile.VPath.data(), vfile.VPath.size());
        vfatwriter << quint8(0);
    }
    vfatsource.close();
    QByteArray vfatcache = vfatsource.buffer();
#ifdef _DEBUG
    SaveByteArrayToFile(((QFile*)file)->fileName() + "_vfat.bin", vfatcache);
#endif
    vfatcache = qCompressEx((uchar*)vfatcache.data(), vfatcache.size(), 9);
    vmbr.VFatCompressed = vfatcache.size();
    vmbr.BinaryStreamSize = fBinarySize;
    vmbr.PlainStreamSize = fPlainCache.size();
    writer << vmbr;
    writer.writeRawData(vfatcache.data(), vfatcache.size());

    int binaryblockoffset = 0x2C + vfatcache.size();
    int plainblockoffset = binaryblockoffset + fBinarySize;

    int binaryblock = greedydiv(fBinarySize, 0x8000);
    int plainblock = greedydiv(fPlainCache.size(), 0x8000);
    quint32 *tailblocks = new quint32[binaryblock + plainblock];
    for (int i = 0; i < binaryblock; i++) {
        tailblocks[i] = binaryblockoffset;
        binaryblockoffset += 0x8000;
    }
    TVTailRec *tailrecs = new TVTailRec[vmbr.FileCount];
    int fileindex = 0;
    foreach (TVFileRec vfile, fVirtualFiles) {
        tailrecs[fileindex].BlockIndex = vfile.Offset / 0x8000 + (vfile.IsBinary?0:binaryblock);
        tailrecs[fileindex].ContentOffset = vfile.Offset % 0x8000;
        fileindex++;
    }
    // binary cache
    //writer.writeRawData(fBinaryCache.data(), fBinaryCache.size());
    foreach (TVFileRec vfile, fVirtualFiles) {
        // UnsortIndex should be increse smoothly
        if (vfile.IsBinary) {
            writer.writeRawData(fUnsortFiles[vfile.UnsortIndex].Content.data(), fUnsortFiles[vfile.UnsortIndex].Content.size());
        }
    }
    fUnsortFiles.clear();

    // plain cache
    int plainindex = binaryblock;
    for (int ipos = 0; ipos < fPlainCache.size(); ipos+= 0x8000) {
        // 32768 
        QByteArray bz2block = qCompressBz((uchar*)(fPlainCache.data() + ipos), qMin(fPlainCache.size() - ipos, 0x8000), 9);
        tailblocks[plainindex] = plainblockoffset;
        writer.writeRawData(bz2block.data(), bz2block.size());
        plainblockoffset += bz2block.size();
        plainindex++;
    }

    QBuffer vtailsource; // use internal cache
    vtailsource.open(QIODevice::WriteOnly);
    QDataStream vtailwriter(&vtailsource);
    vtailwriter.setByteOrder(QDataStream::BigEndian);
    for (int i = 0; i < binaryblock + plainblock; i++) {
        vtailwriter << tailblocks[i];
    }
    for (int i = 0; i < vmbr.FileCount; i++) {
        vtailwriter << tailrecs[i];
    }

    delete tailblocks;
    delete tailrecs;
    vtailsource.close();
    QByteArray vtailcache = vtailsource.buffer();
#ifdef _DEBUG
    SaveByteArrayToFile(((QFile*)file)->fileName() + "_vtail.bin", vtailcache);
#endif
    vtailcache = qCompressEx((uchar*)vtailcache.data(), vtailcache.size(), 9);

    writer.writeRawData(vtailcache.data(), vtailcache.size());
    TVEOMRec veom;
    veom.TailSize = vtailcache.size();
    veom.TailOffset = plainblockoffset;

    writer << veom;

    file->close();

    result = true;

    return result;
}

bool FilenameCompareAsc(const TDummyFileRec& rec1, const TDummyFileRec& rec2) {
    return rec1.VPath.compare(rec2.VPath) < 0;
}

bool OpenBookWriter::FlushUnsortBuffer()
{
    // if you place 2.jpg before 11.jpg bambook will discard page 2~10
    // bambook reader have more strange bug, you must store all files in snb by alpha sort order
    // finally all bug will treat as design

    qSort(fUnsortFiles.begin(), fUnsortFiles.end(), FilenameCompareAsc);

    int index = 0;
    fBinarySize = 0;
    fPlainCache.clear();
    for (TDummyFiles::iterator it = fUnsortFiles.begin(); it != fUnsortFiles.end(); /*it++*/)
    {
        TVFileRec item;
        item.IsBinary = it->IsBinary;
        item.Offset = it->IsBinary?fBinarySize:fPlainCache.size();
        item.Size = it->Content.size();
        item.VPath = it->VPath.toAscii();
        item.UnsortIndex = it->IsBinary?index:-1;
        fVirtualFiles.append(item);

        if (it->IsBinary) {
            //fBinaryCache.append(it->Content);
            fBinarySize += it->Content.size();
            index++;
            it++;
        } else {
            fPlainCache.append(it->Content);
            it->Content.clear();
            it = fUnsortFiles.erase(it);
        }
    }
    
    //fUnsortFiles.clear();

    return true;
}

tagVMBR::tagVMBR()
{
    memcpy(&Magic, "SNBP000B", sizeof(Magic));
    Rev80 = 0x00008000;
    RevA3 = 0x00A3A3A3;
    RevZ1 = RevZ2 = 0;
}

tagVEOMRec::tagVEOMRec()
{
    memcpy(&Magic, "SNBP000B", sizeof(Magic));
}

QDataStream & operator<<( QDataStream &out, const TVMBR &mbr )
{
    out.writeRawData(mbr.Magic, sizeof(mbr.Magic));
    out << mbr.Rev80 << mbr.RevA3 << mbr.RevZ1;
    out << mbr.FileCount << mbr.VFatSize << mbr.VFatCompressed;
    out << mbr.BinaryStreamSize << mbr.PlainStreamSize << mbr.RevZ2;
    return out;
}

QDataStream & operator>>( QDataStream &in, TVMBR &mbr )
{
    in.readRawData(mbr.Magic, sizeof(mbr.Magic));
    in >> mbr.Rev80 >> mbr.RevA3 >> mbr.RevZ1;
    in >> mbr.FileCount >> mbr.VFatSize >> mbr.VFatCompressed;
    in >> mbr.BinaryStreamSize >> mbr.PlainStreamSize >> mbr.RevZ2;
    return in;
}

QDataStream & operator<<( QDataStream &out, const TVFatRec &fat )
{
    out << fat.Attribute << fat.FilenameOffset << fat.OriginalFileSize;
    return out;
}

QDataStream & operator>>( QDataStream &in, TVFatRec &fat )
{
    in >> fat.Attribute >> fat.FilenameOffset >> fat.OriginalFileSize;
    return in;
}

QDataStream & operator<<( QDataStream &out, const TVTailRec &tail )
{
    out << tail.BlockIndex << tail.ContentOffset;
    return out;
}

QDataStream & operator>>( QDataStream &in, TVTailRec &tail )
{
    in >> tail.BlockIndex >> tail.ContentOffset;
    return in;
}

QDataStream & operator<<( QDataStream &out, const TVEOMRec &eom )
{
    out << eom.TailSize << eom.TailOffset;
    out.writeRawData(eom.Magic, sizeof(eom.Magic));
    return out;
}

QDataStream & operator>>( QDataStream &in, TVEOMRec &eom )
{
    in >> eom.TailSize >> eom.TailOffset;
    in.readRawData(eom.Magic, sizeof(eom.Magic));
    return in;
}

QByteArray qCompressBz(const uchar* data, int nbytes, int compressionLevel)
{
    if (nbytes == 0) {
        return QByteArray(4, '\0');
    }
    if (!data) {
        qWarning("qCompressBz: Data is null");
        return QByteArray();
    }
    if (compressionLevel < 1 || compressionLevel > 9)
        compressionLevel = 1;

    unsigned int len = nbytes + nbytes / 4;
    QByteArray bazip;
    int res;
    do {
        bazip.resize(len);
        res = ::BZ2_bzBuffToBuffCompress(bazip.data(), &len, (char*)data, nbytes, compressionLevel, 0, 30);

        switch (res) {
        case BZ_OK:
            bazip.resize(len);
            break;
        case BZ_MEM_ERROR:
            qWarning("qCompressBz: BZ_MEM_ERROR: Not enough memory");
            bazip.resize(0);
            break;
        case BZ_OUTBUFF_FULL:
            len *= 2;
            break;
        }
    } while (res == BZ_OUTBUFF_FULL);

    return bazip;
}

QByteArray qUncompressBz( const uchar* data, int nbytes, int outputbytes )
{
    if (!data) {
        qWarning("qUncompressBz: Data is null");
        return QByteArray();
    }
    if (nbytes <= 0) {
        if (nbytes < 0 || (outputbytes != 0))
            qWarning("qUncompressBz: Input data is corrupted");
        return QByteArray();
    }
    ulong expectedSize = outputbytes;
    unsigned int len = qMax(expectedSize, 1ul);
    QByteArray baunzip;
    int res;
    do {
        baunzip.resize(len);
        res = ::BZ2_bzBuffToBuffDecompress((char*)baunzip.data(), &len, (char*)data, nbytes, 0, 0);

        switch (res) {
        case BZ_OK:
            if ((int)len != baunzip.size())
                baunzip.resize(len);
            break;
        case BZ_MEM_ERROR:
            qWarning("qUncompressBz: BZ_MEM_ERROR: Not enough memory");
            break;
        case BZ_OUTBUFF_FULL:
            len *= 2;
            break;
        case BZ_DATA_ERROR:
            qWarning("qUncompressBz: BZ_DATA_ERROR: Input data is corrupted");
            break;
        }
    } while (res == BZ_OUTBUFF_FULL);

    if (res != BZ_OK)
        baunzip = QByteArray();

    return baunzip;
}

typedef struct tagVFatBundleRec {
    TVFatRec header;
    TVTailRec tail;
    QByteArray VPath;
} TVFatBundleRec;

bool VPathOffsetCompareAsc(const TVFatBundleRec& rec1, const TVFatBundleRec& rec2) {
    return rec1.header.FilenameOffset < rec2.header.FilenameOffset;
}

bool OpenBookReader::LoadSndaBook( QIODevice* file )
{
    bool result = false;

    if (file->isOpen() == false) {
        file->open(QIODevice::ReadOnly);
    }

    QDataStream reader(file);
    reader.setByteOrder(QDataStream::BigEndian);

    TVMBR vmbr;
    reader >> vmbr;

    if (memcmp(&vmbr.Magic, "SNBP000B", sizeof(vmbr.Magic)) != 0) {
        return result;
    }

    QByteArray vfatcache(vmbr.VFatCompressed, Qt::Uninitialized);
    if (reader.readRawData(vfatcache.data(), vfatcache.size()) != vfatcache.size()) {
        return result;
    }
    vfatcache = qUncompressEx1((uchar*)vfatcache.data(), vfatcache.size(), vmbr.VFatSize);
    if (vfatcache.size() != vmbr.VFatSize) {
        return result;
    }

    qint64 binarytailpos = file->pos();
    TVEOMRec veom;
    file->seek(file->size() - sizeof(veom));
    reader >> veom;
    file->seek(veom.TailOffset);
    QByteArray vtailcache(veom.TailSize, Qt::Uninitialized);
    reader.readRawData(vtailcache.data(), vtailcache.size());
    int binaryblock = greedydiv(vmbr.BinaryStreamSize, 0x8000);
    int plainblock = greedydiv(vmbr.PlainStreamSize, 0x8000);
    int tailfullsize = (binaryblock + plainblock) * sizeof(quint32) + vmbr.FileCount * sizeof(TVTailRec);
    vtailcache = qUncompressEx1((uchar*)vtailcache.data(), vtailcache.size(), tailfullsize);
    if (vtailcache.size() != tailfullsize) {
        return result;
    }
    quint32 *tailblocks = new quint32[binaryblock + plainblock];
    TVFatBundleRec *vfatrecs = new TVFatBundleRec[vmbr.FileCount];

    QBuffer vtailsource(&vtailcache);
    vtailsource.open(QIODevice::ReadOnly);
    QDataStream vtailreader(&vtailsource);
    vtailreader.setByteOrder(QDataStream::BigEndian);
    for (int i = 0; i < binaryblock + plainblock; i++) {
        vtailreader >> tailblocks[i];
    }
    for (int i = 0; i < vmbr.FileCount; i++) {
        vtailreader >> vfatrecs[i].tail;
    }
    vtailsource.close();

    //fBinaryCache.resize(vmbr.BinaryStreamSize);
    //reader.readRawData(fBinaryCache.data(), fBinaryCache.size());

    for (int i = 0; i < binaryblock; i++) {
        int blocksize = qMin(0x8000, vmbr.BinaryStreamSize - fBinaryCache.size()); // 0x8000 = tailblocks[i + 1] - tailblocks[i]
        file->seek(tailblocks[i]);
        QByteArray block = file->read(blocksize);
        fBinaryCache.append(block);
    }
    for (int i = 0; i < plainblock; i++) {
        int blocksize = qMin(0x8000, vmbr.PlainStreamSize - fPlainCache.size());
        int bzsize;
        if (i < plainblock - 1) {
            bzsize = tailblocks[binaryblock + i + 1] - tailblocks[binaryblock + i];
        } else {
            bzsize = veom.TailOffset - tailblocks[binaryblock + i];
        }
        file->seek(tailblocks[binaryblock + i]);
        QByteArray block = file->read(bzsize);
        block = qUncompressBz((uchar*)block.data(), block.size(), blocksize);
        fPlainCache.append(block);
    }

    QBuffer vfatsource(&vfatcache);
    vfatsource.open(QIODevice::ReadOnly);
    QDataStream vfatreader(&vfatsource);
    vfatreader.setByteOrder(QDataStream::BigEndian);

    QList < TVFatBundleRec > bundlelist;
    for (int i = 0; i < vmbr.FileCount; i++) {
        vfatreader >> vfatrecs[i].header;
        bundlelist.append(vfatrecs[i]);
    }
    delete [] vfatrecs;
    vfatreader.unsetDevice();
    qSort(bundlelist.begin(), bundlelist.end(), VPathOffsetCompareAsc); // take no effect
    
    QBuffer binarysource(&fBinaryCache);
    binarysource.open(QIODevice::ReadOnly);
    QBuffer plainsource(&fPlainCache);
    plainsource.open(QIODevice::ReadOnly);

    for (int i = 0; i < vmbr.FileCount; i++) {
        vfatsource.seek(bundlelist[i].header.FilenameOffset + vmbr.FileCount * sizeof(TVFatRec)); // 12Byte
        int vpathlen;
        if (i < vmbr.FileCount - 1) {
            vpathlen = bundlelist[i + 1].header.FilenameOffset - bundlelist[i].header.FilenameOffset;
        } else {
            vpathlen = vfatsource.size() - bundlelist[i].header.FilenameOffset;
        }
        bundlelist[i].VPath = vfatsource.read(vpathlen);

        TDummyFileRec dummyfile;
        dummyfile.IsBinary = (bundlelist[i].header.Attribute & 0x40000000) == 0;//(bundlelist[i].header.Attribute == 0x01000000);
        dummyfile.VPath = bundlelist[i].VPath;
        if (dummyfile.IsBinary) {
            // block start from 0
            binarysource.seek(bundlelist[i].tail.BlockIndex * 0x8000 + bundlelist[i].tail.ContentOffset);
            dummyfile.Content = binarysource.read(bundlelist[i].header.OriginalFileSize);
        } else {
            // followed by binary blocks
            plainsource.seek((bundlelist[i].tail.BlockIndex - binaryblock) * 0x8000 + bundlelist[i].tail.ContentOffset);
            dummyfile.Content = plainsource.read(bundlelist[i].header.OriginalFileSize);
        }
        fUnsortFiles.append(dummyfile);
    }
    vfatsource.close();
    binarysource.close();
    plainsource.close();

    fBinaryCache.clear();
    fPlainCache.clear();

    delete [] tailblocks;
    file->close();

    //FillUnsortBuffer();

    result = true;

    return result;
}

bool OpenBookReader::FillUnsortBuffer()
{
    bool result = false;
    return result;
}

bool OpenBookReader::DumpUnsortFiles( QString xmlfolder )
{
    bool result = false;
    
    foreach(const TDummyFileRec& dummyfile, fUnsortFiles) {
        SaveByteArrayToFile(xmlfolder + "/" + dummyfile.VPath, dummyfile.Content);
    }

    return result;
}